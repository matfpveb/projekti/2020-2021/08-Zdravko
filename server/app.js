const express = require('express')
const app = express();
const { urlencoded, json } = require('body-parser');
const patientsRouter = require('./routes/patients')
const preglediRouter = require("./routes/pregledi");
const zakazivanjeRouter = require("./routes/zakazivanje");
const doctorRouter = require('./routes/doctors');
const biopsijaRouter = require('./routes/biopsija');

app.use(json());
app.use(urlencoded({ extended: false }));

//implementacije CORS zastiite
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PATCH, PUT, DELETE');
    
        return res.status(200).json({});
    }
    

    next();
});

app.use('/api/pregledi', preglediRouter);
app.use('/api/patients', patientsRouter);
app.use('/api/zakazivanje', zakazivanjeRouter);

app.use('/api/doctor', doctorRouter);

app.use('/api/biopsija', biopsijaRouter);

app.get("/",(req,res)=>{
    const html = `<h1>Hello World</h1>`;
    res.status(200).send(html);
});

module.exports = app;
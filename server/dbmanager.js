const mysql = require('mysql2/promise');
const fs = require('fs');

const args = fs.readFileSync("db.config",'utf-8').trim().split('\n');
const username = args[0];
const passwd = args[1];

const pacijent = {
    jmbg: '2909998000000',
	lbo: '11111111111',
    Ime : 'Vladimir',
    Prezime: 'Arsenijevic',
    Adresa : 'Potes Loznicko Polje I 48/9',
    Grad : 'Cacak',
    Godiste : 1998,
    Telefon : '0653195200',
    Pol : 'M',
    email: 'arsavm@gmail.com',
};
const pregled = {
	jmbg:2909998999999,
	datum:vreme,
	id_doktora:123,
	izvestaj:"Text"
};

var vreme = new Date().toISOString().slice(0, 19).replace('T', ' ');

const zakazivanje = {
	datum:vreme,
	jmbg: '2909998999999',
	id_doktora: '123',
};

const karton = {
	id:3,
	jmbg:'2909998999999',
	id_doktora:123,
	tip_biopsije:0,
	datum_biopsije:vreme,
	tip_terapije:0,
	alergije:'-',
	datum_dijagnoze_tumora:vreme,
};

const getAllPatientsFromDB = async function(){
	try{ 
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute('SELECT * FROM pacijent;');
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getPatientFromDB = async function(jmbg){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("SELECT * FROM pacijent WHERE jmbg = ?",[jmbg]);
		return rows[0];
	}catch(err){
		console.log(err);
		return false;
	}
};

const addPatientToDB = async function(pacijent){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO pacijent VALUES (?,?,?,?,?,?,?,?,?,?)",Object.values(pacijent));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const removePatientFromDB = async function(jmbg){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("DELETE FROM pacijent WHERE jmbg = ?",[jmbg]);
		//TODO obrisati i kartonng 
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};


const addAccountToDB = async function(account){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO nalog VALUES (?,?, ?)",Object.values(account));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const getTable =async function(name){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from ?", name);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
};

const insertKarton = async function(karton){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO karton(jmbg, id_doktora, tip_biopsije, datum_biopsije, tip_terapije, alergije, datum_dijagnoze_tumora) VALUES (? ,?, ?, ?, ?, ?, ?)",Object.values(karton));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getKartonForPatientFromDB = async function(JMBG){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from pacijent p join karton k on p.jmbg = k.jmbg and k.jmbg = ?",[JMBG]);
		return rows[0];
	}catch(err){
		console.log(err);
		return false;
	}	
}

const editPatientData = async function(pacijent){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("update pacijent set lbo=?,ime=?,prezime=?,adresa=?,grad=?,godiste=?,telefon=?,pol=?,email=? where jmbg=?",[pacijent.lbo,pacijent.ime,pacijent.prezime,pacijent.adresa,pacijent.grad,pacijent.godiste,pacijent.telefon,pacijent.pol,pacijent.email,pacijent.jmbg]);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const getPreglediForDateFromDB = async function(datum){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select pr.jmbg, p.ime, p.prezime from pacijent p join pregledi pr on p.jmbg = pr.jmbg and DATE(pr.datum) = ?",[datum]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}


const getAppointmentsForDateFromDB = async function(datum){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select z.jmbg, p.ime, p.prezime,z.datum,z.id_doktora from pacijent p join zakazivanja z on p.jmbg = z.jmbg and DATE(z.datum) = ?",[datum]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getNextAppointment = async function(jmbg){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from zakazivanja where jmbg = ? and TIMEDIFF(datum,NOW())>0 order by datum asc limit 1",[jmbg]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getZakazivanjaZaDatumKodLekara = async function(datum, ILekara){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const today = new Date();
		const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		datetime = datum + ' ' + time;
		const datum_pom = new Date().toISOString().slice(0, 10).replace('T', ' ');
		var rows,fields;
		if (datum_pom == datum){
			[rows, fields] = await connection.execute("select z.jmbg, p.ime, p.prezime,TIME(z.datum) as vreme,z.id_doktora from pacijent p join zakazivanja z on p.jmbg = z.jmbg where TIMEDIFF(z.datum,?) >=0 and z.datum = datum and z.id_doktora = ?",[datetime, ILekara]);
		}
		else {
			[rows, fields] = await connection.execute("select z.jmbg, p.ime, p.prezime,TIME(z.datum) as datum,z.id_doktora from pacijent p join zakazivanja z on p.jmbg = z.jmbg where DATE(z.datum) = ? and z.id_doktora = ?",[datum, ILekara]);
		}
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}


const getAppointmentsForDoctorFromDB = async function(id){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from zakazivanja where id_doktora = ?",[id]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const zakaziPregled = async function(pregled){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO zakazivanja VALUES (? ,?, ?)",Object.values(pregled));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const odkaziPregled = async function(jmbg,datum){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("delete from zakazivanja where datum = ? and jmbg = ?",[jmbg,datum]);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const getPatientExaminations = async function(jmbg){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
			
		const [rows,fields] = await connection.execute('select datum,id_doktora,anamneza,izvestaj from pregledi where jmbg = ?',[jmbg]);
		//const [rows, fields] = await connection.execute("select * from pacijent p join pregledi pr on p.jmbg = pr.jmbg and p.jmbg = ?",[pacijent.jmbg]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
};

const recordExaminationtoDB = async function(jmbg,ILekara,anamneza,izvestaj){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const vreme = new Date().toISOString().slice(0, 19).replace('T', ' ');
		const [rows, fields] = await connection.execute("INSERT INTO pregledi VALUES (?,?,?,?,?)",[jmbg,vreme,ILekara,anamneza,izvestaj]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
};

const makeNewMedicalRecordToDB = async function(pacijent){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("insert into karton (jmbg,id_doktora,tip_biopsije) values (?,?,?);",[pacijent.jmbg,123,0]);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const addMedicalRecordToDB = async function(record){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO karton VALUES (?,?,?,?,?,?,?,?)",Object.values(record));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const getMedicalRecordFromDB = async function(jmbg){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from pregled where jmbg = ?",[jmbg]);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const editMedicalRecordFromDB = async function(record){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("delete from karton where id = ?",[record.id]);
		addMedicalRecordToDB(record);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const getAllDoctors = async function(){
	try{	
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from doktor d join zaposleni z on z.id = d.id ");
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getAllPatientsForDoctorFromDB = async function(id){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select p.jmbg,p.lbo,p.ime,p.prezime,p.adresa,p.grad,p.godiste,p.telefon,p.email,p.pol,p.email from karton k join pacijent p on p.jmbg = k.jmbg where k.id_doktora = ?",[id]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
};

const getPatientsLozinkaFromDB = async function(JMBG){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select sifra from nalog where jmbg = ?",[JMBG]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
};

const addBiopsijaIntoDB = async function(biopsija){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO biopsija(tip, datum, histopatoloske_vrednosti, tip_tumora, kod_klinickog_stanja) VALUES (?,?,?,?,?)",Object.values(biopsija));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}
const addBiopsijaIntoKarton = async function(JMBG, datum, tip){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("update karton set tip_biopsije = ?, datum_biopsije = ? where jmbg = ?",[tip, datum, JMBG]);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}

const addKlinickoStanjeIntoDB = async function(stanje){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO klinicko_stanje VALUES (?,?)",Object.values(stanje));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}

const addOperacijaToDB = async function(operacija){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO operacija VALUES (?,?,?,?,?)",Object.values(operacija));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}


const getOperationsForDoctor = async function(ILekara){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from operacija where id_doktora = ?",[ILekara]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const addTerapijaTODB = async function(terapija){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO terapija VALUES (?,?,?)",Object.values(terapija));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getKlinickaStanja  = async function() {
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from klinicko_stanje");
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getTerapijeFromDb  = async function() {
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from terapija");
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
}

const getDoctorsIdForPatient  = async function(JMBG) {
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select id_doktora from karton where jmbg = ?",[JMBG]);
		return rows[0];
	}catch(err){
		console.log(err);
		return false;
	}
}

const getDoctorById = async function(id){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select * from doktor where id = ?",[id]);
		return rows[0];
	}catch(err){
		console.log(err);
		return false;
	}
}

const addEmployeeToDB = async function(id,ime,prezime,struka){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO zaposleni VALUES (?,?,?)",[id,ime,prezime]);
		const [rows2, fields2] = await connection.execute("INSERT INTO doktor VALUES (?,?)",[id,struka]);

		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}

const addEmployeeAccountToDB = async function(nalog){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("INSERT INTO nalog_zaposleni VALUES (?,?,?)",Object.values(nalog));
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
}


const getDoctorsLozinkaFromDB = async function(id){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("select sifra from nalog_zaposleni where id = ?",[id]);
		return rows;
	}catch(err){
		console.log(err);
		return false;
	}
};

const changeDoctor = async function(JMBG, ILekara){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("update karton set id_doktora = ? where jmbg = ?",[ILekara, JMBG]);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};

const addTerapijaToKarton = async function(JMBG, tip){
	try{
		const connection = await mysql.createConnection({
			host:'localhost',
			user: username,
			password: passwd,
			database: 'zdravko'
			});
		const [rows, fields] = await connection.execute("update karton set tip_terapije = ? where jmbg = ?",[tip, JMBG]);
		return true;
	}catch(err){
		console.log(err);
		return false;
	}
};
//setTimeout(async function(){ let x = await getAllPatientsForDoctorFromDB("123");
//				console.log(x); }, 1000);
//setTimeout(async function(){ await editMedicalRecordFromDB(karton);}, 1000);
//getPatientExaminations(pacijent);
//zakaziPregled(pregled);
//odkaziPregled(pacijent);
//getTable("pacijent");
//addPatientToDB(pacijent);
//editPatiendData(pacijent);
//getAllPatientsForDoctorFromDB();

module.exports = {getAllPatientsFromDB,getPatientFromDB,addPatientToDB,removePatientFromDB,editPatientData,
	zakaziPregled,odkaziPregled,getAppointmentsForDoctorFromDB,getPatientExaminations,recordExaminationtoDB,
	makeNewMedicalRecordToDB,editMedicalRecordFromDB,addMedicalRecordToDB,getMedicalRecordFromDB,
	getAllPatientsForDoctorFromDB, getPatientsLozinkaFromDB, addAccountToDB, getPreglediForDateFromDB, getNextAppointment,
	getKartonForPatientFromDB, getAllDoctors, insertKarton, addBiopsijaIntoDB, addBiopsijaIntoKarton,addKlinickoStanjeIntoDB,
	 addOperacijaToDB, addTerapijaTODB, getOperationsForDoctor, getKlinickaStanja, getTerapijeFromDb, getDoctorsIdForPatient,getAppointmentsForDateFromDB,
	getDoctorById,addEmployeeToDB,addEmployeeAccountToDB,getDoctorsLozinkaFromDB, changeDoctor,addTerapijaToKarton, getZakazivanjaZaDatumKodLekara};

const biopsija = require('../models/biopsija');

const unesiBiopsiju = async (req, res, next) => {
    const {tip, datum, histopatoloske_vrednosti, tip_tumora, kod_klinickog_stanja, JMBG} = req.body;
    try{
        if( !datum || !histopatoloske_vrednosti || !tip_tumora || !JMBG){
                const error = new Error("Niste prosledili odgovarajuce vrednosti u Body-ju");
                error.status = 400;
                throw error;
            }
        const isAdded = await biopsija.unesiBiopsiju(tip, datum, histopatoloske_vrednosti, tip_tumora, kod_klinickog_stanja);
        if(isAdded){
            const isAddedToKarton = await biopsija.unesiUkarton(JMBG, datum, tip);
            if(isAddedToKarton)
            res.status(201).json({
                "added" : true
                });
            else{
                res.status(201).json({
                    "added" : false
                    });
            }
        }else{
            const error = new Error("Greska u unosu u bazu");
            error.status = 400;
            throw error;
        }   
    }catch(err){
        next(err);
    }
}

module.exports = {unesiBiopsiju};

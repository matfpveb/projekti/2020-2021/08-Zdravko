const patients = require("../models/patients");
const pregledi = require("../models/pregledi");
const mailservice = require("../models/mailservice");
const jwt = require("../utils/jwt")
//const authetication = require("../routes/authetication")

const getAllPatients = async (req, res, next) =>{
  try{
    let users = await patients.getAllUsers();
    console.log("Iz controlera");
    console.log(users);
    if(users){
      res.status(200).json(users);
    }else{
      const error = new Error("Baza je vratila undefined");
      error.status = 400;
      throw error;  
    }      
  }catch(err){
    next(err);
  }
}

const getAllPatientsForDoctor = async (req, res, next) => {
  let ILekar = req.params.ILekar;
  console.log(ILekar);
  console.log("-------------------------------")
  try{
    if(!ILekar){
      console.log("Nema identifikatora za doktora");
      const error = new Error("Niste prosledili parametar ILekara");
      error.status = 400;
      throw error;
    }
    console.log(`Ovo je u kontoroleru ${ILekar}`);
    let patientsOfDoctor = await patients.getAllPatientsForDoctor(ILekar);
    if(patientsOfDoctor){
      console.log(patientsOfDoctor);
      res.status(200).json(patientsOfDoctor);
    }else{
      const error = new Error("Baza je vratila false");
      error.status = 400;
      throw error;
    }
  }catch(err){
    next(err);
  }
}

const addNewUser = async (req, res, next) => {
    const { JMBG, LBO, ime, prezime, adresa, grad, godiste, telefon, pol, email, lozinka, ILekara,} = req.body;
    try{
      if (
        !JMBG || !LBO||
        !ime || !prezime ||
        !adresa || !grad ||
        !godiste || !telefon ||
        !pol || !email || !lozinka || !ILekara
      ) {
        const error = new Error("Dostavite potrebne info");
        error.status = 400;
        throw error;
      } 
        const jwt = await patients.addNewUser(JMBG, LBO, ime, prezime, adresa, grad, godiste, telefon, pol, email, lozinka);
        const makeKarton = await patients.makeKarton(JMBG, parseInt(ILekara));
        mailservice.sendMailTo(email,"Uspesno napravljen nalog",`Dragi/a ${ime},\nuspesno ste napravili nalog na Zdravku!`);
        
        res.status(201).json({
          token : jwt,
        });
        
    }catch(err){
      next(err)
    }
}

const editUser = async (req, res, next) => {
  const user = req.body;
  try{
    if (!user) {
        const error = new Error("Nije dostavljen user");
        error.status = 400;
        throw error;
    } else {
        const isEdited = patients.editUser(user);
        if (isEdited) {
          res.status(200).json(isEdited);
        } else {
          const error = new Error("Nije obrisan u bazi");
          error.status = 400;
          throw error;
        }
    }
}catch(err){
  next(err)
}
}

const deleteUser = async (req, res, next) => {
    const { JMBG } = req.body;
    try{
      if (!JMBG) {
          const error = new Error("Nije dostavljen JMBG");
          error.status = 400;
          throw error;
      } else {
          const isDeleted = patients.deleteUser(JMBG);
          if (isDeleted) {
            res.status(200).json();
          } else {
            const error = new Error("Nije obrisan u bazi");
            error.status = 400;
            throw error;
          }
      }
  }catch(err){
    next(err)
  }
}

const getPatientByJMBG = async (req, res, next) => {
  let JMBG = req.params.JMBG;
  try{
    if(!JMBG){
      const error = new Error("Nije poslat JMBG parametar");
      error.status(400);
      throw err
    }else{
      let user = await patients.getPatientByJMBG(JMBG);
      if(user){
        res.status(200).json(user);
      }else{
        const error = new Error("U bazi nema takvog usera");
        error.status = 404;
        throw error;
      }
    }
  }catch(err){
    next(err);
  }
}

const loginUser = async (req, res, next) => {
  const {JMBG, lozinka} = req.body;
  try{
    if(!JMBG || !lozinka){
      const error = new Error("Niste poslali JMBG ili lozinku");
      error.status = 400;
      throw error;
    }
    console.log(JMBG, lozinka);
    const lozinkaUser = await patients.isPaswordVaild(JMBG, lozinka);
    if(!lozinkaUser){
      const error = new Error("Anauthorized");
      error.status = 401;
      throw error;
    }else{
      const patientData = await patients.getPatientByJMBG(JMBG);
      user = {JMBG : JMBG, ime : patientData.ime, prezime : patientData.prezime};
      const accessToken = jwt.generateJWT(user);
      console.log(accessToken);
      res.status(200).json({
        token : accessToken
      });
    }
  }catch(err){
    next(err);
  }
}

const getKartonZaPacijenta = async (req, res, next) => {
  const JMBG = req.params.JMBG;
  try{
    if(!JMBG){
      const error = new Error("Niste poslali JMBG");
      error.status = 400;
      throw error;
    }
    const katron = await patients.getKartonZaPacijenta(JMBG);
    if(katron){
      res.status(200).json(katron);
    }else{
      const error = new Error("U bazi nema kartona");
      error.status = 400;
      throw error;
    }
  }catch(err){
    next(err);
  }
}

const getPatientsDoctor = async (req, res, next) => {
  const JMBG = req.JMBG;
  console.error(JMBG);
  try{
    const ILekara = await patients.getDoctorsIdForPatient(JMBG);
    if(ILekara){
      res.status(200).json(ILekara);
    }else{
      const error = new Error("Nije baza vratila odgovor");
      error.status = 400;
      throw error;
    }
  }catch(err){

  }
}

module.exports = {addNewUser, deleteUser,editUser, getAllPatients, getAllPatientsForDoctor, 
                    getPatientByJMBG, loginUser, getKartonZaPacijenta, getPatientsDoctor
                  };

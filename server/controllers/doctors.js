const doctors = require('../models/doctors');
const jwt = require("../utils/jwt")

const addNewEmployee = async (req, res, next) => {
    const { id,ime,prezime,struka,lozinka} = req.body;
    try{
      if (!id || !ime || !prezime || !struka || !lozinka) {
        const error = new Error("Dostavite potrebne info");
        error.status = 400;
        throw error;
      } 
        const jwt = await doctors.addNewEmployee(id,ime,prezime,struka,lozinka);
        res.status(201).json({
          token : jwt,
        });
        
    }catch(err){
      next(err)
    }
}

const login = async (req, res, next) => {
    const {id, lozinka} = req.body;
    try{
      if(!id || !lozinka){
        const error = new Error("Niste poslali id ili lozinku");
        error.status = 400;
        throw error;
      }
      const lozinkaDoktor = await doctors.isPaswordVaild(id, lozinka);
      if(!lozinkaDoktor){
        const error = new Error("Anauthorized");
        error.status = 401;
        throw error;
      }else{
        const employeeData = await doctors.getDoctorById(id);
        doctor = {id:id, ime : employeeData.ime, prezime : employeeData.prezime};
        const accessToken = jwt.generateJWT(doctor);
        res.status(200).json({
          token : accessToken
        });
      }
    }catch(err){
      next(err);
    }
  }
const getAllDoctors = async (req, res, next) => {
    try{
        const allDoctors = await doctors.getAllDoctors();
        if(allDoctors){
            res.status(200).json({
                "lista" :  allDoctors
            });
        }else{
            const error = new Error("Nije uspeo zahtev u bazi");
            error.status = 400;
            throw error;
        }
    }catch(err){
        next(err)
    }
}

const addKlinickoStanje = async (req, res, next) => {
    const {kod, ime} = req.body;
    try{
        if(!kod || !ime){
            const error = new Error("Niste poslali ime i kod");
            error.status = 400;
            throw error;
        }
        const isAdded = await doctors.addKlinickoStanje(kod, ime);
        if(isAdded){
            res.status(201).json();
        }else{
            res.status(400).json();
        }
    }catch(err){
        next(err);
    }
}

const addOperacija = async (req, res, next) => {
    const ILekara = req.params.ILekara;
    const id_doktora = parseInt(ILekara);
    
    const {datum, jmbg_pacijenta, tip_zahvata,propratni_pomocni_zahvar} = req.body;
    try{
        if(!datum || !id_doktora || !jmbg_pacijenta || !tip_zahvata || !propratni_pomocni_zahvar){
            const error = new Error("Niste poslali podatke u body-ju");
            error.status = 400;
            throw error;    
        }
        const isAdded = await doctors.addOperacija(datum, id_doktora, jmbg_pacijenta, tip_zahvata,propratni_pomocni_zahvar);
        if(isAdded){
            res.status(201).json({added : isAdded});
        }else{
            const error  = new Error("Baza nije upisala podatke");
            error.status = 400;
            throw error;
        }
    }catch(err){
        next(err);
    }
}

const addTerapija = async (req, res, next) => {
    const {tip, broj_ciklusa, grupa_lekova, JMBG} = req.body;
    try{
        if(!tip || !broj_ciklusa || !grupa_lekova || !JMBG){
            const error = new Error("Niste poslali podatke u bod-ju");
            error.status = 400;
            throw error;
        }
        const isAddeed = await doctors.addTerapija(tip, broj_ciklusa, grupa_lekova);
        if(isAddeed){
            const isAddedToKarton = await doctors.addTerapijaToKarton(JMBG, tip);
            if(isAddedToKarton){
                res.status(201).json({
                "added" : true
                });
            }else{
                res.status(200).json({
                    "added" : false
                })
            }
        }else{
            res.status(200).json({
                "added" : false
            })
        }
    }catch(err){
        next(err);
    }
}

const getOperacijeForLekar = async (req, res, next) => {
    const ILekara = req.params.ILekara;
    try{
        if(!ILekara){
            const error = new Error("Niste poslali ILekara");
            error.status = 400;
            throw error;
        }
        const operacije = await doctors.getOperacijeForLekar(ILekara);
        if(operacije){
            res.status(200).json(operacije);
        }else{
            const error = new Error("Baza nije vratila oodg");
            error.status = 400;
            throw error;
        }
    }catch(err){
        next(err);
    }
}


const getKlinickoStanje = async (req, res, next) => {
    try{
        const KlinickaStanja = await doctors.getKlinickaStanja();
        if(KlinickaStanja){
            res.status(200).json(KlinickaStanja);
        }else{
            const error = new Error("Baza nije vratila oodg");
            error.status = 400;
            throw error;
        }
    }catch(err){
        next(err);
    }
}

const getTerapije = async (req, res, next) => {
    try{
        const terapije = await doctors.getTerapije();
        if(terapije){
            res.status(200).json(terapije);
        }else{
            const error = new Error("Baza nije vratila oodg");
            error.status = 400;
            throw error;
        }
    }catch(err){
        next(err);
    }
}

const getPredhodniPregledi = async(req, res, next) => {
    const jmbg = req.params.jmbg;
    try{
        const pregledi = await doctors.getPredhodniPregledi(jmbg);
        if(pregledi){
            res.status(200).json(pregledi);
        }else{
            const error = new Error("Baza nije vratila odg");
            error.status = 400;
            throw error;
        }
    }catch(err){
    next(err);
    }
}

const changeDoctor = async (req, res, next) =>{
    const JMBG = req.JMBG;
    const {ILekara} = req.body;
    try{
        if(!ILekara){
            const error = new Error("Niste poslali idLekara kroz body");
            error.status = 400;
            throw error;
        }
        isChanged = await doctors.changeDoctor(JMBG, ILekara);
        if(isChanged){
            res.status(200).json({changed : true});
        }else{
            const error = new Error("Nisu zamenjeni podaci");
            error.status = 400;
            throw error; 
        }
    }catch(err){
        next(err);
    }
}
module.exports = {getAllDoctors, addKlinickoStanje, addOperacija, addTerapija, getOperacijeForLekar, 
    getKlinickoStanje, getTerapije,addNewEmployee, changeDoctor,login,getPredhodniPregledi};
const pregledi = require("../models/pregledi");

const zakaziPregledK = async (req, res, next) => {
    const { datum, jmbg, id_doktora} = req.body;
    try{
      if (
        !datum ||
        !jmbg ||
        !id_doktora
      ) {
        const error = new Error("Niste poslali podatke u body-u nepohodne");
        error.status = 400;
        throw error;
      } else {
        const isAdded = await pregledi.zakaziPregled(datum, jmbg, id_doktora);
        if (isAdded) {
          //pregledi.posaljiMailPacijentu(lbo);
          res.status(201).json();
        } else {
          const error = new Error("Nema vezu sa bazom");
          error.status = 400;
          throw error;
        }
      }
    }catch(err){
      next(err);
    }
}

const otkaziPregledK = async (req, res, next) => {
    const { datum, jmbg, id_doktora} = req.body;
    try{
      if(!datum || !jmbg || !id_doktora){
        const error = new Error("Niste poslali neophodne podatke u body-ju")
        error.status = 400;
        throw error;
      }
      else{
        pregled = {datum: datum,
          jmbg: jmbg,
          id_doktora: id_doktora
        };
        const isDeleted = await pregledi.otkaziPregled(pregled);
        if (isDeleted) {
          res.status(200).json();
          console.log("Obrisan pregled");
        } else {
          res.status(404).json();
          console.log("Neuspesan zahtev");
        }
      }
    }catch(err){
      next(err);
    }
}

const izlistajPregledeZaDatum = async (req, res, next) => {
  const datum = req.params.datum;
  try{
    if(!datum){
      const error = new Error("Niste prosedili datum zakazivanja");
      error.status = 400;
      throw error;
    }
    const pregleiB = await pregledi.preglediZaDatum(datum);
    if(pregleiB){
      res.status(200).json(pregleiB)
    }else{
      const error = new Error("Greska u bazi podataka");
      error.status = 400;
      throw error;
    }
  }catch(err){
    next(err);
  }
}

const newAnamneza = async (req, res) => {
  try{
    const {LBO, DatumDijagnoze, Anamneza} = req.body;
    var isSuccessful = await pregledi.addAnamneza(LBO, DatumDijagnoze, Anamneza);
    if(!isSuccessful){
      //console.log("Greska u dodavanju anamneze");
      res.status(400).json();
    }
    res.status(201).json(isSuccessful);
  }catch(err){
    res.status(400).json
  }
}

const zabeleziPregled = async(req,res) => {
  try{
    const {jmbg,anamneza,izvestaj} = req.body;
    const ILekara = req.id;
    var isSuccessful = await pregledi.zabeleziPregled(jmbg,ILekara,anamneza,izvestaj);
    if(!isSuccessful){
      console.log("Greska u dodavanju pregleda");
      res.status(400).json();
    }
    res.status(201).json(isSuccessful);
  }catch(err){
    res.status(400).json
  }
}
module.exports = {zakaziPregledK, otkaziPregledK, izlistajPregledeZaDatum, newAnamneza, zabeleziPregled};
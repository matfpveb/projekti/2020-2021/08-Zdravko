const zakazivanja = require("../models/zakzivanje");
const mailservice = require("../models/mailservice");
const { getPatientByJMBG } = require("../models/patients");

const prijavaZaZakazivanje = async (req,res,next) => {
    const {datum,ILekara} = req.body;
    const JMBG = req.JMBG;
    try{
        if( !JMBG || !datum || !ILekara){
            const err = new Error("Niste poslali podatke");
            err.status = 400;
            throw err;
        }
        const isAdded = await zakazivanja.zakaziPregled(JMBG,datum,ILekara);
        if (isAdded){
            user = await getPatientByJMBG(JMBG);
            mailservice.sendMailTo(user.email,"Upseno zakazivanje",`Uspesno ste zakazali pregled kod vaseg lekara za:\n ${datum}.`);
            res.status(201).json({odobreno:true});
        }
        else{
            res.status(401).json({odobreno:false});
        } 
    }catch(err){
        next(err);
    }
}

const otkaziZakazivanje = async (req, res,next) => {
    const datum = req.body;
    const JMBG = req.JMBG;
    try{
        if( !JMBG || !datum){
            const err = new Error("Niste poslali podatke");
            err.status = 400;
            throw err;
        }
        const isDeleted = await zakazivanja.odkaziPregled(JMBG,datum);
        if (isDeleted)
            res.status(201).json({odobreno:true});
        else{
            res.status(401).json({odobreno:false});
        } 
    }catch(err){
        next(err);
    }
}

const getZakazivanjaDoktor = async (req,res,next) => {
    const ILekara = req.params.ILekara;
    try{
        if (!ILekara){
            const err = new Error("Niste poslali parametar");
            err.status = 400;
            throw err;
        }
        const result = await zakazivanja.getZakazivanjaDoktor(ILekara);
        if (result)
            res.status(200).json({"hasAppointments" : true,
                                "resoponse" : result});
        else{
            res.status(200).json({"hasAppointments" : false, "response" : []});
        }
    }catch(err){
        next(err);
    }
}

const izlistajZakazivanjaZaDatum = async (req, res, next) => {
    const datum = req.body;
    try{
      if(!datum){
        const error = new Error("Niste prosedili datum zakazivanja");
        error.status = 400;
        throw error;
      }else{
      const pregleiB = await zakazivanja.zakazivanjaZaDatum(datum);
      if(pregleiB){
        res.status(200).json(pregleiB)
      }else{
        const error = new Error("Greska u bazi podataka");
        error.status = 400;
        throw error;
      }
    }
    }catch(err){
      next(err);
    }
  }
const zakazaniPreglediZaDatumKodLekara = async function (req, res, next){
    const {datum, ILekara} = req.body;
    try{
        if(!datum || !ILekara){
            const error =  new Error("Nsite prosledili podatke kroz body");
            error.status = 400;
            throw error;
        }
        const zakazani = await zakazivanja.getZakazivanjaZaDatumKodLekara(datum, ILekara);
        if(zakazani){
            res.json({
                "hasApp" : true,
                "lista" : zakazani});
        }else{
            res.status(200).json({
                "hasApp" : false,
                "lista" : []
            });
        }
    }catch(err){
        next(err)
    }
}

const sledeciPregled = async (req, res, next) => {
    try{
        const jmbg = req.JMBG;
        const pregled = await zakazivanja.sledeciPregled(jmbg);
        if(pregled){
            res.status(200).json(pregled)
        }else{
            const error = new Error("Nema podataka o zakazivanje");
            error.status = 404;
            throw error;
        }
    }catch(err){
      next(err);
    }
  }
module.exports = {prijavaZaZakazivanje, otkaziZakazivanje,getZakazivanjaDoktor,izlistajZakazivanjaZaDatum, zakazaniPreglediZaDatumKodLekara,sledeciPregled};
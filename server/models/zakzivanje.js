const db = require('../dbmanager')

const zakaziPregled = async function(JMBG,datum,ILekara){
    const zakazivanje = {datum:datum,jmbg:JMBG,id_doktora:ILekara};
    try{
        const provera = sledeciPregled(jmbg)[0].datum;
        if (provera.substring(0,11) == datum.substring(0,11))
            return false;
        return await db.zakaziPregled(zakazivanje);
    }catch(err){
        console.log(err);
        return false;
    }
}

const odkaziPregled = async function(JMBG,datum){
    try{
        return await db.odkaziPregled(JMBG,datum);
    }catch(err){
        console.log(err);
        return false;
    }
}

const getZakazivanjaDoktor = async function(ILekara){
    try{
        return await db.getAppointmentsForDoctorFromDB(ILekara);
    }catch(err){
        console.log(err);
        return false;
    }

}

const zakazivanjaZaDatum = async function(datum){
    try{
        return await db.getAppointmentsForDateFromDB(datum);
    }catch(err){
        console.log(err);
        return false;
    }
}

const getZakazivanjaZaDatumKodLekara = async function(datum, ILekara){
    try{
        return await db.getZakazivanjaZaDatumKodLekara(datum, ILekara);
    }catch(err){
        console.log(err);
        return false;
    }
}

const sledeciPregled = async function(jmbg){
    try{
        return await db.getNextAppointment(jmbg);
    }catch(err){
        console.log(err);
        return false;
    }
}
module.exports = {odkaziPregled, zakaziPregled,getZakazivanjaDoktor,zakazivanjaZaDatum, getZakazivanjaZaDatumKodLekara, sledeciPregled};
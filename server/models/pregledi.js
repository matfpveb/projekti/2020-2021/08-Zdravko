const mailService = require("./mailservice");
const patients = require("./patients");
const db = require("../dbmanager")

let pregledi = [
    {
        id : 123124214,
        idZakzivanja: 4123432,
        datumPregleda: "12.3.2021",
        lekar: 123432154132,
        lbo: 1234567890123,
        vremePregleda: "13:20"
    }
]


const zakaziPregled = async function(datum, jmbg, id_doktora){
    try{
        pregled = {datum: datum,
            jmbg: jmbg,
            id_doktora: id_doktora
        };
        return await db.zakaziPregled(pregled);
    }catch(err){
        console.log(err);
        return false;
    }
}

const otkaziPregled = async function(pregled){
    try{
        return await db.odkaziPregled(pregled)
    }catch(err){
        console.log(err);
        return false;
    }
}

const zabeleziPregled = async function(jmbg,ILekara,anamneza,izvestaj){
    try{
        return await db.recordExaminationtoDB(jmbg,ILekara,anamneza,izvestaj);
    }catch(err){
        console.log(err);
        return false;
    }
}

const posaljiMailPacijentu = function(lbo){
    //TODO fali f-ja u patients da se nadje user po lbo-u
    console.log('Poslati mail');
}

const preglediZaDatum = async function(datum){
    try{
        return await db.getPreglediForDateFromDB(datum);
    }catch(err){
        console.log(err);
        return false;
    }
}

const addAnamneza = function(LBO, DatumDijagnoze, Anamneza){
    /*pacijent = getPatientByJMBG(JMBG);
    pacijent.Anamneza = Anamneza;
    db.editPatientData(pacijent);*/
    try{
        //return await db.addAnamneza(JMBG,Anamneza);
        return false; //TODO treba napraviti fju zavrsi pregled koja upisuje i anamnezu i izvestaj
    }catch(err){
        console.log(err);
        return false;
    }
  }
module.exports = {zakaziPregled, otkaziPregled,zabeleziPregled, posaljiMailPacijentu, preglediZaDatum, addAnamneza, zabeleziPregled};

const db = require('../dbmanager');
const patients = require('../models/patients');
const jwtUtil = require("../utils/jwt")
const bcrypt = require('bcrypt');


const getAllDoctors = async function(){
    try{
        const allDoctors = await db.getAllDoctors();
        return allDoctors;
    }catch(err){
        console.log((err));
        return false;
    }
}

const getDoctorById = async function(id){
    try{
        const Doctor = await db.getDoctorById(id);
        return Doctor;
    }catch(err){
        console.log((err));
        return false;
    }
}

const getPredhodniPregledi = async function(jmbg){
  try{
      const pregledi = await db.getPatientExaminations(jmbg);
      return pregledi;
  }catch(err){
      console.log((err));
      return false;
  }
}

const addKlinickoStanje = async function (kod, ime){
    try{
        const isAdded = await db.addKlinickoStanjeIntoDB({
            kod : kod,
            ime : ime
        })
        return isAdded;
    }catch(err){
        console.log(err);
        return false;
    }
}
 
const addOperacija = async function(datum, id_doktora, jmbg_pacijenta, tip_zahvata,propratni_pomocni_zahvar){
    try{
        return await db.addOperacijaToDB({
            datum : datum,
            id_doktora :id_doktora,
            jmbg_pacijenta : jmbg_pacijenta,
            tip_zahvata : tip_zahvata,
            propratni_pomocni_zahvar, propratni_pomocni_zahvar
        });
    }catch(err){

    }
}

const addTerapija =  async function(tip, broj_ciklusa, grupa_lekova){
    try{
        return await db.addTerapijaTODB({
            tip : tip,
            broj_ciklusa : broj_ciklusa,
            grupa_lekova : grupa_lekova
        });
    }catch(err){
        console.error(err);
        return false;
    }
}

const getOperacijeForLekar = async function (ILekara){
    try{
        return await db.getOperationsForDoctor(ILekara);
    }catch(err){
        console.error(err);
        return false;
    }
}

const getKlinickaStanja = async function(){
    try{
        return await db.getKlinickaStanja();
    }catch(err){
        console.error(err);
        return false;
    }
}

const getTerapije = async function(){
    try{
        return await db.getTerapijeFromDb();
    }catch(err){
        console.error(err);
        return false;
    }
}

const addNewEmployee = async function(id,ime,prezime,struka,lozinka){
    try{
      const hashObj = patients.getPassword(lozinka);
      const nalog = {id:id, sifra : (await hashObj).hashPass, salt: (await hashObj).salt};
      isAddedUser = await db.addEmployeeToDB(id,ime,prezime,struka);
      isAddedNalog = await db.addEmployeeAccountToDB(nalog);
      if(isAddedUser && isAddedNalog){
        return await getEmployeeJWTByID(id);
      }
      else{
        throw Error("Nije uneto u bazu");
      }
    }catch(err){
      console.log(err);
      return false;
    }
  }

  async function getEmployeeJWTByID(id){
    try{
      const doctor = await getDoctorById(id);
      if(!doctor){
        throw new Error("Doctor with this ${id} does not exist");
      }
      return jwtUtil.generateJWT({
        id: doctor.id
      });
    }catch(err){
      console.log(err);
    }
  }

  const isValidPassword = async function (lozinka, hashPass){
    return await bcrypt.compare(lozinka, hashPass);
  }

  const isPaswordVaild = async function(id, lozinka){
    try{
      const hashPass = await db.getDoctorsLozinkaFromDB(id);
      return await isValidPassword(lozinka, hashPass[0].sifra);
    }catch(err){
      console.log(err);
      return false;
    }
  }

const changeDoctor = async function(JMBG, ILekara){
    try{
      return await db.changeDoctor(JMBG, ILekara);
    }catch(err){
      console.log(err);
      return false;
    }
}

const addTerapijaToKarton = async function(JMBG, tip){
  try{
    return await db.addTerapijaToKarton(JMBG, tip);
  }catch(err){
    console.error(err);
    return false;
  }
}
module.exports = {getAllDoctors, addKlinickoStanje, addOperacija, addTerapija, getOperacijeForLekar, getKlinickaStanja, getTerapije,addNewEmployee,
    getDoctorById, isPaswordVaild, changeDoctor, addTerapijaToKarton,getPredhodniPregledi};
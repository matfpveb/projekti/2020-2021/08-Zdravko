const db = require('../dbmanager');

const unesiBiopsiju = async function (tip, datum, histopatoloske_vrednosti, tip_tumora, kod_klinickog_stanja){
    try{
        return await db.addBiopsijaIntoDB({
            tip : tip,
            datum : datum,
            histopatoloske_vrednosti :histopatoloske_vrednosti,
            tip_tumora : tip_tumora,
            kod_klinickog_stanja : kod_klinickog_stanja
        });
    }catch(err){
        console.log(err);
        return false;
    }
}

const unesiUkarton = async function(JMBG, datum, tip){
    try{
        return await db.addBiopsijaIntoKarton(JMBG, datum, tip);
    }catch(err){
        console.error(err);
        return false;
    }
}
module.exports ={ unesiBiopsiju , unesiUkarton};
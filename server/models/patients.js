const db = require("../dbmanager");
const bcrypt = require("bcrypt")
const jwtUtil = require("../utils/jwt")
const SALT_ROUNDS = 10;

const getPassword = async (lozinka) => {
  const salt = await bcrypt.genSalt(SALT_ROUNDS);
  const hashPass = await bcrypt.hash(lozinka, salt);
  return{salt: salt, hashPass : hashPass};
}

const isValidPassword = async function (lozinka, hashPass){
  return await bcrypt.compare(lozinka, hashPass);
}

async function getUserJWTByJMBG(JMBG){
  try{
    const patient = await getPatientByJMBG(JMBG);
    if(!patient){
      throw new Error("Patient with this ${JMBG} does not exist");
    }
    return jwtUtil.generateJWT({
      JMBG: patient.jmbg
    });
  }catch(err){
    console.log(err);
  }
}

const getAllUsers = async function () {
  //console.log(`Ovo je iz modela ${arr}`);
  try{
    return await db.getAllPatientsFromDB();
  }catch(err){
    console.log(err);
    return undefined;
  }
}

const getPatientByLBO =  function (LBO){
  let foundUser = arr.filter((user) => user.LBO == LBO);
  return foundUser.length > 0 ? foundUser[0] : null;
}

const getPatientByJMBG = async function(JMBG){
  try{
    return await db.getPatientFromDB(JMBG);
  }catch(err){
    console.log(err);
    return false;
  }
}

const getAllPatientsForDoctor = async function(ILekar){
  try{
    return await db.getAllPatientsForDoctorFromDB(ILekar);
  }catch(err){
    console.log(err);
    return false;
  }
}

//Ovo bi trebalo samo medicinska sestra da moze da uradi
const addNewUser = async function(JMBG, LBO, ime, prezime, adresa, grad, godiste, telefon, pol, email, lozinka){
    try{
      const newUser = {
        JMBG: JMBG,
        LBO: LBO,
        Ime: ime, 
        Prezime: prezime,
        Adresa: adresa,
        Grad: grad,
        Godiste: godiste,
        Telefon: telefon,
        Pol: pol,
        Email: email
      };
      const hashObj = getPassword(lozinka);
      const nalog = {JMBG : JMBG, sifra : (await hashObj).hashPass, salt: (await hashObj).salt};
      isAddedUser = await db.addPatientToDB(newUser);
      isAddedNalog = await db.addAccountToDB(nalog);
      if(isAddedUser && isAddedNalog){
        return await getUserJWTByJMBG(JMBG);
      }
      else{
        throw Error("Nije uneto u bazu");
      }
    }catch(err){
      console.log(err);
      return false;
    }
  }
  
  const editUser = async (User) => {
    try{
      console.log(User);
      return await db.editPatientData(User);
    }catch(err){
      console.log(err);
      return false;
    }
  	
  }

  
  const deleteUser = async (JMBG) => {
    try{
      return await db.removePatientFromDB(JMBG);
    }catch(err){
      console.log(err);
      return false;
    }
  };

  const addMedicalRecord = (record) => {
    db.addMedicalRecordToDB(record);
  }
  
  const isPaswordVaild = async function(JMBG, lozinka){
    try{
      const hashPass = await db.getPatientsLozinkaFromDB(JMBG);
      return await isValidPassword(lozinka, hashPass[0].sifra);
    }catch(err){
      console.log(err);
      return false;
    }

  }

  const getKartonZaPacijenta = async function(JMBG){
    try{
      const katron = await db.getKartonForPatientFromDB(JMBG);
      return katron;
    }catch(err){
      console.log(err);
      return false;
    }
  }

  const makeKarton = async function(JMBG, ILekara){
    try{
      katron = {
        jmbg : JMBG,
        id_doktora : ILekara,
        tip_biopsije : 0,
        datum_biopsije : "1999-01-01 02:22:22",
        tip_terapije : 0,
        alergije : "-",
        datum_dijagnoze_tumora : '1999-01-01 02:22:22'
      }
      return await db.insertKarton(katron);
    }catch(err){
      console.log(err);
      return false;
    }
  }

  const getDoctorsIdForPatient = async function(JMBG){
    try{
      return await db.getDoctorsIdForPatient(JMBG);
    }catch(err){
      console.error(err);
      return false;
    }
  }
module.exports = {getAllUsers, addNewUser, deleteUser, getAllPatientsForDoctor, getPatientByJMBG,addMedicalRecord, 
                    getPatientByLBO, isPaswordVaild, getKartonZaPacijenta, makeKarton, getDoctorsIdForPatient,getPassword,editUser};

const express = require('express');
const controllerPatients = require('../controllers/patients');
const controllerPregledi = require('../controllers/pregledi');
const controller = require('../controllers/doctors');
const authentication = require('../utils/authetication');
const router = express.Router();

router.get('/:ILekar', authentication.isAuthenticatedDoctor,controllerPatients.getAllPatientsForDoctor);
router.get('/', controller.getAllDoctors);
router.get('/operacije/:ILekara', authentication.isAuthenticatedDoctor, controller.getOperacijeForLekar);
router.get('/klinicko/stanje', authentication.isAuthenticatedDoctor, controller.getKlinickoStanje);
router.get('/terapije/postojece', authentication.isAuthenticatedDoctor, controller.getTerapije);
router.get('/pregledi/svi',authentication.isAuthenticatedDoctor,controller.getPredhodniPregledi);

router.post('/', controller.addNewEmployee);
router.post('/login',controller.login);


router.post('/:JMBG', authentication.isAuthenticatedDoctor,controllerPregledi.newAnamneza);
router.post('/klinicko/:ILekara', authentication.isAuthenticatedDoctor ,controller.addKlinickoStanje);
router.post('/operacija/:ILekara', authentication.isAuthenticatedDoctor, controller.addOperacija);
router.post("/terapija/:ILekara", authentication.isAuthenticatedDoctor, controller.addTerapija);


router.patch('/', authentication.isAuthenticated, controller.changeDoctor);

module.exports = router;


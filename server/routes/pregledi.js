const express = require('express');
const controller = require('../controllers/pregledi');
const authentication = require('../utils/authetication');
const router = express.Router();

router.post('/', authentication.isAuthenticated,controller.zakaziPregledK);
router.post('/zabelezi',authentication.isAuthenticatedDoctor,controller.zabeleziPregled);

router.delete('/', authentication.isAuthenticated,controller.otkaziPregledK);

router.get('/:datum', controller.izlistajPregledeZaDatum);

module.exports = router;
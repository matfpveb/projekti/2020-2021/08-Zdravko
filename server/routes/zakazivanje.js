const express = require('express');
const controller = require('../controllers/zakazivanje');
const authentication = require('../utils/authetication');
const router = express.Router();

router.post('/',authentication.isAuthenticated ,controller.prijavaZaZakazivanje);
router.post('/zakazivanja/doktor/datum', authentication.isAuthenticated, controller.zakazaniPreglediZaDatumKodLekara);

router.delete('/',authentication.isAuthenticated, controller.otkaziZakazivanje);

router.get('/sledece',authentication.isAuthenticated,controller.sledeciPregled);
router.get('/:ILekara',authentication.isAuthenticated,controller.getZakazivanjaDoktor);
router.get('/zakazivanja', authentication.isAuthenticated,controller.izlistajZakazivanjaZaDatum);


module.exports = router;
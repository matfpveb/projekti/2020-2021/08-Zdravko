const express = require('express');
const controller = require('../controllers/patients');


const authentication = require('../utils/authetication');

const router = express.Router();


router.get('/', authentication.isAuthenticated, controller.getAllPatients)
router.get('/:JMBG', authentication.isAuthenticated,controller.getPatientByJMBG);
router.get('/karton/:JMBG', authentication.isAuthenticated, controller.getKartonZaPacijenta);
router.get('/doctor/Ilekara', authentication.isAuthenticated, controller.getPatientsDoctor)

router.delete('/', authentication.isAuthenticated,controller.deleteUser);

router.post('/edit',authentication.isAuthenticated,controller.editUser);
router.post('/login',controller.loginUser);
router.post('/', controller.addNewUser);

module.exports = router;

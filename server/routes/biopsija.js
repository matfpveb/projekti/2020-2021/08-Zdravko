const express = require('express');
const controller = require('../controllers/biopsija');
const authentication = require('../utils/authetication');

const router = express.Router();

router.post('/', authentication.isAuthenticated, controller.unesiBiopsiju);

module.exports = router;
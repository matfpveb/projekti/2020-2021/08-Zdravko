CREATE DATABASE IF NOT EXISTS zdravko;

USE zdravko;

CREATE TABLE IF NOT EXISTS zaposleni(
	id smallint not null,
	ime varchar(30) not null,
	prezime varchar(30) not null,
	primary key (id)
	);

CREATE TABLE IF NOT EXISTS doktor(
	id smallint not null,
	struka varchar(30),
	primary key (id),
	foreign key (id) references zaposleni(id)
	);

CREATE TABLE IF NOT EXISTS pacijent(
	jmbg char(13) not null,
	lbo char(11),
	ime varchar(30) not null,
	prezime varchar(30) not null,
	adresa varchar(50),
	grad varchar(20),
	godiste smallint,
	telefon varchar(13),
	pol char(1) not null,
	email varchar(30),
	primary key(jmbg)
	);

CREATE TABLE IF NOT EXISTS nalog(
	jmbg char(13) not null,
	sifra varchar(100) not null,
	salt varchar(100),
	primary key(jmbg),
	foreign key(jmbg) references pacijent(jmbg)
	);

CREATE TABLE IF NOT EXISTS nalog_zaposleni(
	id smallint not null,
	sifra varchar(100) not null,
	salt varchar(100),
	primary key(id),
	foreign key (id) references doktor(id)
);

CREATE TABLE IF NOT EXISTS klinicko_stanje(
	kod varchar(20) not null,
	ime varchar(50),
	primary key (kod)
	);

CREATE TABLE IF NOT EXISTS operacija(
	datum datetime not null,
	id_doktora smallint not null,
	jmbg_pacijenta char(13) not null,
	tip_zahvata varchar(20),
	propratni_pomocni_zahvar varchar (20),
	primary key (datum,id_doktora),
	foreign key (jmbg_pacijenta) references pacijent(jmbg),
	foreign key (id_doktora) references doktor(id)
	);

CREATE TABLE IF NOT EXISTS biopsija(
	id smallint not null AUTO_INCREMENT,
	tip varchar(20) not null,
	datum datetime,
	histopatoloske_vrednosti varchar(50),
	tip_tumora varchar(20),
	kod_klinickog_stanja varchar(20) not null,
	primary key (id),
	foreign key (kod_klinickog_stanja) references klinicko_stanje(kod),
	index(tip)
	);
	
CREATE TABLE IF NOT EXISTS terapija(
	tip varchar(40) not null,
	broj_ciklusa tinyint,
	grupa_lekova varchar(20),
	primary key (tip)
	);

CREATE TABLE IF NOT EXISTS karton(
	id smallint not null AUTO_INCREMENT,
	jmbg char(13) not null,
	id_doktora smallint not null,
	tip_biopsije varchar(20) not null,
	datum_biopsije datetime,
	tip_terapije varchar(20),
	alergije varchar(100),
	datum_dijagnoze_tumora datetime,
	primary key (id),
	foreign key (jmbg) references pacijent(jmbg),
	foreign key (tip_biopsije) references biopsija(tip),
	foreign key (tip_terapije) references terapija(tip),
	foreign key (id_doktora) references doktor(id)
	);
	
CREATE TABLE IF NOT EXISTS pregledi(
	jmbg char(13) not null,
	datum datetime not null,
	id_doktora smallint not null,
	anamneza varchar(2000),
	izvestaj varchar(2000),
	primary key(jmbg,datum),
	foreign key(jmbg) references pacijent(jmbg),
	foreign key(id_doktora) references doktor(id)
	);	
	
CREATE TABLE IF NOT EXISTS zakazivanja(
	datum datetime not null,
	jmbg char(13) not null,
	id_doktora smallint not null,
	primary key(datum,jmbg),
	foreign key(jmbg) references pacijent(jmbg),
	foreign key(id_doktora) references doktor(id)
	);
	
CREATE OR REPLACE VIEW pacijent_pacijent AS
select p.ime as Ime,p.prezime as Prezime,k.jmbg as JMBG,z.ime as Ime_Doktora,k.tip_terapije as Terapija
from pacijent p join karton k on p.jmbg = k.jmbg
join doktor d on d.id = k.id_doktora
join zaposleni z on z.id = d.id;

CREATE OR REPLACE VIEW pacijent_doktor AS
select p.ime as Ime,p.prezime as Prezime,k.jmbg as JMBG,z.ime as Ime_Doktora,k.tip_biopsije as Tip_Biopsije,datum_biopsije as Datum_Biopsije,tip_terapije as Terapija,alergije as Alergije
from pacijent p join karton k
on p.jmbg = k.jmbg
join doktor d on k.id_doktora = d.id
join zaposleni z on z.id = d.id;


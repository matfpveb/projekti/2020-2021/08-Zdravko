USE zdravko;

insert into pacijent values
(2909998999999,'123','Vladimir','Arsenijevic','Studentski Trg 13','Beograd',1998,'06511111111','M','arsavm@gmail.com'),
(1505998999999,'456','Pera','Peric','Studentski Trg 11','Beograd',1998,'06511111123','M','varsavm@gmail.com'),
(0102998999999,'789','Marija','Stojic','Svetog Save 50','Uzice',1997,'06411111111','Z','marija@gmail.com')
;

insert into zaposleni values
(123,'Petar','Rondovic')
;

insert into doktor values
(123,'Opsta')
;

insert into klinicko_stanje values
(0,0),
('MB','Pneumonia'),
('DR','Disaster related'),
('MF','Monoclonal gammopathy ')
;

insert into biopsija(tip,kod_klinickog_stanja) values
(0,0),
('inciziona','DR'),
('eksciziona','DR'),
('inciziona','MB')
;

insert into terapija values
(0,0,''),
('psiho','50c'),
('fizikalna','27c'),
('kognitivno-ponasajuca','1k 3c')
;

insert into karton(jmbg,id_doktora,tip_biopsije) values
(2909998999999,'123',0)
;

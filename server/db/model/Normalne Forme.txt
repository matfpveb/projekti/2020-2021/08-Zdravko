Pacijent(JMBG,tip_terapije,istorija_pacijenta,ime,prezime,adresa,grad,godiste,telefon,pol,email,anamneza,ddt);
Operacija(DATUM, ID DOKTORA,jmbg_pacijenta,tip zahvata, propratni pomocni zahvat);
Terapija(TIP,broj ciklusa, grupa lekova);
Biopsija(ID,tip,datum,kod_klinickog_stanja,histopatoloske vrednosti,tip_tumora);
Klinicko_stanje(KOD,ime);
Tip_tumora(TIP);
Istorija Pacijenta(ID,JMBG,tip_biopsije,datum_biopsije,informacije o pregledima);


1. NF
Pacijent(JMBG,tip_terapije,istorija_pacijenta,ime,prezime,adresa,grad,godiste,telefon,pol,email,anamneza,ddt);
Operacija(DATUM, ID DOKTORA,jmbg_pacijenta,tip zahvata, propratni pomocni zahvat);
Terapija(TIP,broj ciklusa, grupa lekova);
Biopsija(ID,tip,datum,kod_klinickog_stanja,histopatoloske vrednosti,tip_tumora);
Klinicko_stanje(KOD,ime);
Tip_tumora(TIP);
Istorija Pacijenta(ID,JMBG,tip_biopsije,datum_biopsije,informacije o pregledima);


----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

Pacijent - kandidati za kljuc:
JMBG, istorija_pacijenta, email

FZ:
JMBG -> ime, prezime, adresa, grad, godiste,telefon,pol,email, anamnzea
istorija_pacijenta -> ddt

2. NF
R1 = (JMBG,ime, prezime, adresa, grad, godiste,telefon,pol,email, anamnzea)
R2 = (JMBG,tip_terapije,istorija_pacijenta,ddt)

R3 = (istorija_pacijenta,ddt)
R4 = (JMBG,tip_terapije,istorija_pacijenta)

R1 X R3 X R4

3.NF
jeste

BC NF
jeste

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

Operacija(DATUM, ID_DOKTORA,jmbg_pacijenta,tip zahvata, propratni pomocni zahvat);
Operacija - kandidati za kljuc:
(Datum,ID doktora), (DATUM, JMBG_PACIJENTA)

FZ:
Datum,ID doktora -> jmbg_pacijenta, tip zahvata, propratni pomocni zahvat
DATUM,JMBG_PACIJENTA -> ID_DOKTORA, tip zahvata, propratni pomocni zahvat

2. NF
jeste
3. NF
jeste
BC NF
jeste

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

Biopsija(ID,tip,datum,kod_klinickog_stanja,histopatoloske vrednosti,tip_tumora);
Biopsija - kandidati za kljuc:
ID

FZ: 
ID-> tip,datum,kod_klinickog stanja,histopatoloske_vrednosti,tip_tumora
hispatoloske_vrednosti->tip_tumora
hispatoloske_vrednosti->kod_klinickog stanja

2. NF
jeste
3. NF
R1 = (hispatoloske_vrednosti,tip_tumora)
R2 = (hispatoloske_vrednosti,kod_klinickog stanja)
R3 = (ID,tip,datum,histopatoloske_vrednosti)
BC NF
jeste

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

Istorija Pacijenta(ID,JMBG,tip_biopsije,datum_biopsije,informacije o pregledima);
istorija_pacijenta - kandidati za kljuc:
ID, JMBG

FZ:
ID -> JMBG,tip_biopsije,datum_biopsije,informacije o pregledima
JMBG -> ID, tip_biopsije,datum_biopsije,informacije o pregledima

2. NF
jeste
3. NF
jeste
BC NF
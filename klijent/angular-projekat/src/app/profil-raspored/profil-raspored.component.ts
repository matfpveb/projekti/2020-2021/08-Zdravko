import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { AppointmentService } from '../services/appointment.service';
import { DoctorService } from '../services/doctor.service';
import { UserInfoComponent } from '../user-info/user-info.component';
import { Chart } from '../_models/chart.model';

@Component({
  selector: 'app-profil-raspored',
  templateUrl: './profil-raspored.component.html',
  styleUrls: ['./profil-raspored.component.css']
})
export class ProfilRasporedComponent implements OnInit, OnDestroy {

  private docSub : Subscription;
  private appSub : Subscription;

  public IzvestajForm : FormGroup;
  public patientsChart : Chart;

  constructor(private dService : DoctorService, private appService :  AppointmentService,private router : Router) {
    if(!this.dService.hasPatientAtTheMoment){
      window.alert("Pacijent nije izabran!");
      router.navigateByUrl('/doctor/home');
    }else{
      this.docSub = this.dService.getPatientsChart().subscribe((response : Chart)=> {
        this.patientsChart = response;
        //console.log(this.patientsChart);
      });  
    }
    UserInfoComponent.pocetak = false;

  }

  ngOnInit(): void {
    this.IzvestajForm = new FormGroup({
      "anamneza" : new FormControl('', [Validators.required]),
      "izvestaj" : new FormControl('', [Validators.required])
    }); 
  }

  ngOnDestroy() : void{
    if(this.dService.hasPatientAtTheMoment){
      this.dService.unsetPatient();
    }
    if(this.docSub){
      this.docSub.unsubscribe();
    }

    if(this.appSub){
      this.appSub.unsubscribe();
    }
  }

  public saveAppointment() : void{
    if(this.IzvestajForm.invalid){
      window.alert('Moreta popunite izvestaj u celosti!');
      return;
    }
    
    const data = this.IzvestajForm.value;
    const anamneza : string = data.anamneza;
    const izvestaj : string = data.izvestaj;

    this.appSub = this.appService.addPregled(this.patientsChart.jmbg, anamneza, izvestaj).subscribe((response : boolean) => {
      if(response){
        window.alert("Usepsno ste uneli podatke o pregledu pregled");
        //TODO:pozovi da se obrise pregled iz liste zakazanih
        this.router.navigateByUrl('/doctor/raspored');
      }
    });    
    //console.log(anamneza);
    //console.log(izvestaj);
  }

}

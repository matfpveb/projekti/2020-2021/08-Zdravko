import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilRasporedComponent } from './profil-raspored.component';

describe('ProfilRasporedComponent', () => {
  let component: ProfilRasporedComponent;
  let fixture: ComponentFixture<ProfilRasporedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilRasporedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilRasporedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

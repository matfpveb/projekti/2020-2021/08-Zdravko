import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AppointmentService } from '../services/appointment.service';
import { DoctorService } from '../services/doctor.service';
import { UserService } from '../services/user.service';
import { Appointment } from '../_models/appointment.model';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnDestroy {

  private UserIsLogged = false;
  private sub : Subscription;
  private appSub : Subscription;
  private setApp :Subscription;

  public SelecteDateForm : FormGroup;
  public dateSelected : boolean = false;
  public ILekara : string;
  public termini;
  public datum : string;

  constructor(private userService : UserService, private appService : AppointmentService) {
    
    this.userService.sendUserDataIfExists();
    if(this.userService.isLoggedIn){
      this.UserIsLogged = true;
      console.log("Bilo sat");
    }

    this.sub  = this.userService.getUsersDoctor().subscribe((response : {"id_doktora" : number}) => {
      this.ILekara = String(response.id_doktora);
      console.log(this.ILekara);
    });
    
    //danasnji datum
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    this.datum =yyyy+ '-' + mm + '-' + dd;


    //znaci prvo se pita server i onda se slobodni termini ovde upisu,
    //ovo ces vrv da pozivas negde drugde, ali da bi inicijalno imali nesto u ovom nizu
    //mozes da namestis da trazi termine za trenutni datum, a posle kada pacijent odabere
    //datum neka radi normalno
    this.termini = ["9:00", "9:15","9:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30" ,"14:00"];

  }

  ngOnInit(): void {
    this.SelecteDateForm = new FormGroup({
      "datum" : new FormControl('', [Validators.required])
    });
  }

  ngOnDestroy() : void {
    if(this.sub){
      this.sub.unsubscribe();
    }
    if(this.appSub){
      this.appSub.unsubscribe();
    }
    if(this.setApp){
      this.setApp.unsubscribe();
    }
  }

  public zakaziTermin(termin:string):void{
    const data = this.SelecteDateForm.value;
    let datumZakazivanja = data.datum;

    if(datumZakazivanja == ""){
      window.alert("Niste uneli datum");
      return; 
    }

    //window.alert("Zakazali ste termin za "+ datumZakazivanja +" u "+termin);
    //console.log(termin.length);
    if(termin.length == 4){
      datumZakazivanja = datumZakazivanja + " 0" + termin + ":00";
    }else{
      datumZakazivanja = datumZakazivanja + " " + termin + ":00";
    }
    
    this.setApp = this.appService.setAnAppointment(datumZakazivanja, this.ILekara).subscribe((response : {odobreno : boolean}) => {
      if(!response.odobreno){
        window.alert("Termin je vec zakazan. Molimo pokusajte neki od preeostalih termina");
        this.setChoosenDate();
      }
      else{
        window.alert("Uspesno ste zakazali termin");
        this.setChoosenDate();
      }
    });
  }

  public setChoosenDate() : void {
    const data = this.SelecteDateForm.value;
    if(data.datum == ""){
      window.alert("Niste izabrali datum!");
      return;
    }

    this.dateSelected = true;
    this.datum = data.datum;
    //console.log(this.datum);

    //TESTIRANJE
    //this.ILekara = "12";
    //this.datum = "2021-05-28";
    this.termini = ["9:00", "9:15","9:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30" ,"14:00"];
    
    this.appSub = this.appService.getAppointmentsForDoctor(this.datum, this.ILekara).subscribe((response : {"hasApp" : boolean, "lista" : Appointment[]}) => {
      const listSize = response.lista.length;
      //const hours : string = response.lista[0].datum;
      //console.log(response.lista);
      let i = 0;
      while(listSize > i){
        let hoursSize = response.lista[i].datum.length;
        //console.log(response.lista[i].datum.length);
        let termin : string;
        if(hoursSize == 7){
          termin = response.lista[i].datum.substring(0, 4);
        }else if(hoursSize == 8){
          termin = response.lista[i].datum.substring(0, 5);
        }
        //console.log(termin);
        var index = this.termini.indexOf(termin);
        //console.log(index);
        if (index !== -1) {
          //console.log("ok");
          this.termini.splice(index, 1);
        }
        i = i+1;
      }
    })

  }


}

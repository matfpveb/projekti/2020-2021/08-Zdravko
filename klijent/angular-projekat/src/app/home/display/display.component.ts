import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/_models/user.model';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DisplayComponent implements OnInit {
  public user : User = null;
  
  constructor(private uService : UserService) {
    this.user = this.uService.currentUser;
   }

  ngOnInit(): void {
  }

}

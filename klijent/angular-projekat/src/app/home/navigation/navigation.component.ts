import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { UserInfoComponent } from 'src/app/user-info/user-info.component';
import { User } from 'src/app/_models/user.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit{
  public user : User = null;
  public profil : boolean;
 
  constructor(private userService : UserService, private router : Router) {

    this.user = this.userService.currentUser;
    this.profil = false;
    UserInfoComponent.pocetak=false;
  }

  ngOnInit(): void {
  }

  public logout(){
    this.userService.logout();
    UserInfoComponent.pocetak=true;
  }

  public showProfile() : void{
    this.router.navigateByUrl('/profil');
  }

  
}

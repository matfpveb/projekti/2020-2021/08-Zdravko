import { Injectable } from '@angular/core';
import { IJWTTokenData } from 'src/app/_models/jwt-token-data';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private static readonly USER_TOKEN_ID : string = 'USER_JWT_TOKEN';

  constructor() { }

  public getDataFromTokne() : IJWTTokenData{
    const token = this.getToken();
    if(!token){
      return null;
    }

    const payLoadString : string = token.split('.')[1];
    const userDataJson : string = atob(payLoadString);
    const payLoad : IJWTTokenData = JSON.parse(userDataJson);

    return payLoad;
  }

  public setToken(jwt :string) : void{
    localStorage.setItem(JwtService.USER_TOKEN_ID , jwt);
  }
  public getToken() : string | null{
    const token:string | null = localStorage.getItem(JwtService.USER_TOKEN_ID);
    if(!token){
      return null;
    }else{
      return token;
    }
  }

  public removeToken() : void{
    localStorage.removeItem(JwtService.USER_TOKEN_ID);
  }
}

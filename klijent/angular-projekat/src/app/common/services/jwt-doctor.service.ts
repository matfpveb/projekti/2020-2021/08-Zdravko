import { Injectable } from '@angular/core';
import { IJwtTokenDataDoctor } from 'src/app/_models/jwt-doctor-data' 

@Injectable({
  providedIn: 'root'
})
export class JwtDoctorService {
  private static readonly EMPLOYEE_TOKEN_ID : string = 'EMPLOYEE_JWT_TOKEN';

  constructor() { }

  public getDataFromTokne() : IJwtTokenDataDoctor{
    const token = this.getToken();
    if(!token){
      return null;
    }

    const payLoadString : string = token.split('.')[1];
    const userDataJson : string = atob(payLoadString);
    const payLoad : IJwtTokenDataDoctor = JSON.parse(userDataJson);

    return payLoad;
  }

  public setToken(jwt :string) : void{
    localStorage.setItem(JwtDoctorService.EMPLOYEE_TOKEN_ID , jwt);
  }
  public getToken() : string | null{
    const token:string | null = localStorage.getItem(JwtDoctorService.EMPLOYEE_TOKEN_ID);
    if(!token){
      return null;
    }else{
      return token;
    }
  }

  public removeToken() : void{
    localStorage.removeItem(JwtDoctorService.EMPLOYEE_TOKEN_ID);
  }
}

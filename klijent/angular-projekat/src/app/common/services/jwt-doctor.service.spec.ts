import { TestBed } from '@angular/core/testing';

import { JwtDoctorService } from './jwt-doctor.service';

describe('JwtDoctorService', () => {
  let service: JwtDoctorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JwtDoctorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

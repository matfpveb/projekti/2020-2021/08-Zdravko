import { TestBed } from '@angular/core/testing';

import { DoctorAuthenticatedGuard } from './doctor-authenticated.guard';

describe('DoctorAuthenticatedGuard', () => {
  let guard: DoctorAuthenticatedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(DoctorAuthenticatedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

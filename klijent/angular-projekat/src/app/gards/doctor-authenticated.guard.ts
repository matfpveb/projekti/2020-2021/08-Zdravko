import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DoctorService } from '../services/doctor.service';

@Injectable({
  providedIn: 'root'
})
export class DoctorAuthenticatedGuard implements CanActivate {
  
  constructor(private doctorService : DoctorService, private router : Router){}
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
    this.doctorService.sendUserDataIfExists();
    if(this.doctorService.isLoggedIn){
      return true;
    }

    return this.router.createUrlTree(['/doctor']);

  }
  
}

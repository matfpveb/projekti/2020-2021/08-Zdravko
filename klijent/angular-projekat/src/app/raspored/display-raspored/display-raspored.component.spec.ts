import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayRasporedComponent } from './display-raspored.component';

describe('DisplayRasporedComponent', () => {
  let component: DisplayRasporedComponent;
  let fixture: ComponentFixture<DisplayRasporedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayRasporedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayRasporedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

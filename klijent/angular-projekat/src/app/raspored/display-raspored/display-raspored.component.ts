import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppointmentService } from 'src/app/services/appointment.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { Appointment } from 'src/app/_models/appointment.model';

@Component({
  selector: 'app-display-raspored',
  templateUrl: './display-raspored.component.html',
  styleUrls: ['./display-raspored.component.css']
})
export class DisplayRasporedComponent implements OnInit, OnDestroy {

  private appSub : Subscription;

  public SelecteDateForm : FormGroup;
  public dateSelected : boolean = false;
  public termini : Appointment[];
  public datum : string;

  constructor(private appointmentService : AppointmentService, private router : Router, private dService : DoctorService) { 
    //danasnji datum
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    this.datum =yyyy + '-' + mm + '-' + dd;
    
    //this.termini = ["09:00", "10:00", "11:30", "12:00", "12:30", "13:00", "14:00"];

  }

  ngOnInit(): void {
    this.SelecteDateForm = new FormGroup({
      "datum" : new FormControl('', [Validators.required])
    });
  }

  ngOnDestroy() : void{
    if(this.appSub){
      this.appSub.unsubscribe();
    }
  }

  public setChoosenDate() : void{
    const data = this.SelecteDateForm.value;
    if(data.datum == ""){
      window.alert("Niste izabrali datum!");
      return;
    }

    this.dateSelected = true;
    this.datum = data.datum;
    //console.log(this.datum);

    this.appSub = this.appointmentService.getScheduledAppointments(this.datum).subscribe((response : {"hasApp" : boolean, "lista" : Appointment[]}) => {
      this.termini = response.lista;
      //console.log(this.termini);
    });

  }

  public startAppointment(jmbg : string) : void{
    this.dService.setPatient(jmbg);
    this.router.navigateByUrl('doctor/see-patient');
    //console.log(jmbg);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayKartoniComponent } from './display-kartoni.component';

describe('DisplayKartoniComponent', () => {
  let component: DisplayKartoniComponent;
  let fixture: ComponentFixture<DisplayKartoniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayKartoniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayKartoniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

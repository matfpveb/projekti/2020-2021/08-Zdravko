import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { DoctorService } from 'src/app/services/doctor.service';
import { User } from 'src/app/_models/user.model';
import { UserData } from 'src/app/_models/userData.model';

@Component({
  selector: 'app-display-kartoni',
  templateUrl: './display-kartoni.component.html',
  styleUrls: ['./display-kartoni.component.css']
})
export class DisplayKartoniComponent implements OnInit, OnDestroy {

  private docSub : Subscription;

  public patients : UserData[];
  

  constructor(private dService : DoctorService, private router : Router) {
    this.docSub = this.dService.getAllMyPatientes().subscribe((response : UserData[]) => {
      this.patients = response;
      //console.log(response);
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy() : void{
    if(this.docSub){
      this.docSub.unsubscribe();
    }
  }

  public startAppointment(jmbg : string) : void{
    this.dService.setPatient(jmbg);
    this.router.navigateByUrl('doctor/see-patient');
    //console.log(jmbg);
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DoctorService } from '../services/doctor.service';
import { UserService } from '../services/user.service';
import { Doctor } from '../_models/doctor.model';
import { User } from '../_models/user.model';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

static pocetak:Boolean=true;

constructor() { 
  }

  ngOnInit(): void {
  }

  ngOnDestroy() : void{
  
  }

  getPocetak():Boolean{
    return UserInfoComponent.pocetak;
  }

  
}

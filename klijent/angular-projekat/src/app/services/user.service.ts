import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { JwtService } from '../common/services/jwt.service';
import { IJWTTokenData } from '../_models/jwt-token-data';
import { User } from '../_models/user.model';
import { UserData } from '../_models/userData.model'

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private readonly userSubject : Subject<User> = new Subject;
  public readonly user : Observable<User> = this.userSubject.asObservable();
  public isLoggedIn : boolean = false;

  private readonly url = {
    login: 'http://localhost:3002/api/patients/login',
    register: 'http://localhost:3002/api/patients/', 
    getMyDoctor : 'http://localhost:3002/api/patients/doctor/Ilekara',
    changeDoc : 'http://localhost:3002/api/doctor',
    edit : 'http://localhost:3002/api/patients/edit'
  }
  

  public currentUser: User = null;

  constructor(private http:HttpClient, private jwtService : JwtService) {
  }

  public register(LBO: string, JMBG: string, ime: string,
                  prezime: string, adresa: string, grad: string,
                  telefon: string, pol: string, email: string,
                  anamneza: string, lozinka: string, ILekara : string, alergije : string, godiste : string) : Observable<User>{
                    console.log("Pozvan register iz user service-a")
                    //Server ocekuje JMBG, LBO, ime, prezime, adresa, grad, godiste, telefon, pol, email, anamneza
                    const body = {"JMBG" : JMBG, "LBO" : LBO, "ime" : ime, "prezime" : prezime, 
                                  "adresa" : adresa, "grad" : grad, "godiste" : godiste, "telefon" : telefon,
                                   "pol" : pol, "email" : email, "lozinka" : lozinka, "ILekara" : ILekara};
                    
                    //Kada dodamo u server jwt token onda ovde dodamo post<{token:string}>
                    return this.http.post<{token : string}>(this.url.register, body).pipe(
                      tap((response : {token : string}) => this.jwtService.setToken(response.token)),
                      catchError((error : HttpErrorResponse) => this.handleError(error)),
                      map((response : {token :string}) => this.sendUserDataIfExists())
                    ); 

                  }
  

  public login(JMBG: string, lozinka: string) : Observable<User>{
      const body = {"JMBG" : JMBG, "lozinka" : lozinka};
      return this.http.post<{token : string}>(this.url.login, body).pipe(
        tap((response : {token : string}) => this.jwtService.setToken(response.token)),
        catchError((error : HttpErrorResponse) => this.handleError(error)),
        map((response : {token :string}) => this.sendUserDataIfExists())
      );
  }

  public logout() : void{
    this.jwtService.removeToken();
    this.userSubject.next(null);
  }

  public get currentUserValue(): User{
    return this.currentUser;
  }

  public sendUserDataIfExists() : User {
    const payLoad : IJWTTokenData = this.jwtService.getDataFromTokne();
    if(!payLoad){
      return null;
    }

    const newUser =  new User(payLoad.JMBG, payLoad.ime, payLoad.prezime);
    this.currentUser = new User(payLoad.JMBG, payLoad.ime, payLoad.prezime);
    this.userSubject.next(newUser);
    if(newUser === null){
      this.isLoggedIn = false;
    }else{
      this.isLoggedIn = true;
    }
    return newUser;   
  }

  private handleError(error : HttpErrorResponse) : Observable<{token :string}>{
    const serverError : {message : string, status : number, stack :string} = error.error;
    window.alert(`Doslo je do greske: ${serverError.message}. Povratni kod servera: ${serverError.status
    }`);

    return of({token : this.jwtService.getToken()});
  }

  private mapResponseToUser(response : {token :string}) : User{
    this.jwtService.setToken(response.token);
    return this.sendUserDataIfExists();
  }

  public getUserInfo() : Observable<UserData>{
   const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    
   const getUrl : string = this.url.register + "/" + this.currentUser.JMBG; 
   const obsUserData : Observable<UserData> = this.http
              .get<UserData>(getUrl , { headers });
   return obsUserData;
  }

  public getUsersDoctor() : Observable<{"id_doktora" : number}>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);

    return this.http.get<{"id_doktora" : number}>(this.url.getMyDoctor, {headers});

  }

  public changeDoctor(ILekara : string) : Observable<{changed : boolean}>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    const body = {"ILekara" : ILekara};

    return this.http.patch<{changed : boolean}>(this.url.changeDoc, body, {headers});
  }

  public editUserData(jmbg: string, lbo : string, ime: string, prezime: string, adresa : string, grad : string, godiste : string, telefon : string, pol : string, email : string,) : Observable<boolean>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    const body = {"jmbg": jmbg, "lbo" : lbo, "ime": ime, "prezime": prezime, "adresa" : adresa, "grad" : grad, "godiste" : godiste, "telefon" : telefon, "pol" : pol, "email" : email};

    return this.http.post<boolean>(this.url.edit, body, {headers});
  }
}
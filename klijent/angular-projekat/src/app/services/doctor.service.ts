import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { Doctor } from '../_models/doctor.model';
import { catchError, map, tap } from 'rxjs/operators'
import { IJwtTokenDataDoctor } from '../_models/jwt-doctor-data';
import { JwtDoctorService } from '../common/services/jwt-doctor.service';
import { DoctorData } from '../_models/docotr-data.model';
import { JwtService } from '../common/services/jwt.service';
import { UserData } from '../_models/userData.model';
import { Chart } from '../_models/chart.model';
 
@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  private readonly doctorSubject : Subject<Doctor> = new Subject;
  public readonly doctor : Observable<Doctor> = this.doctorSubject.asObservable(); 
  private currentDoctor : Doctor = null;
  public isLoggedIn = false;

  private readonly urls = {
    base : 'http://localhost:3002/api/doctor',
    login : 'http://localhost:3002/api/doctor/login',
    operation : 'http://localhost:3002/api/doctor/operacija',
    biopsija : 'http://localhost:3002/api/biopsija',
    terapija : 'http://localhost:3002/api/doctor/terapija',
    getChart : 'http://localhost:3002/api/patients//karton'
  }

  private currentlySeeingPatient : string = null;
  public hasPatientAtTheMoment : boolean = false;
 
  constructor(private http:HttpClient, private jwtService : JwtDoctorService, private jwtUserService : JwtService){} 

  public login(ILekara : string, lozinka : string) : Observable<Doctor>{
    const body = {"id" : ILekara, "lozinka" : lozinka};
    return this.http.post<{token : string}>(this.urls.login, body).pipe(
      tap((response : {token : string}) => this.jwtService.setToken(response.token)),
      catchError((error : HttpErrorResponse) => this.handleError(error)),
      map((response : {token :string}) => this.sendUserDataIfExists())
    );
  }

  public logout() : void{
    this.jwtService.removeToken();
    this.doctorSubject.next(null);
  }

  public sendUserDataIfExists() : Doctor {
    const payLoad : IJwtTokenDataDoctor = this.jwtService.getDataFromTokne();
    if(!payLoad){
      return null;
    }

    const newDoctor =  new Doctor(payLoad.id);
    this.doctorSubject.next(newDoctor);
    this.currentDoctor = newDoctor;
    if(newDoctor === null){
      this.isLoggedIn = false;
    }else{
      this.isLoggedIn = true;
    }
    return newDoctor;   
  }

  private handleError(error : HttpErrorResponse) : Observable<{token :string}>{
    const serverError : {message : string, status : number, stack :string} = error.error;
    window.alert(`Doslo je do greske: ${serverError.message}. Povratni kod servera: ${serverError.status
    }`);

    return of({token : this.jwtService.getToken()});
  }

  public getAllMyPatientes() : Observable<UserData[]>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);

    const getUrl : string = this.urls.base + "/" + this.currentDoctor.id; 
    const obsResponse : Observable<UserData[]> = this.http
      .get<UserData[]>(getUrl, {headers});
    
    return obsResponse;
  }

  public addOperationInfo(datum : string, JMBG : string, tipZahvata : string, propratniZahvat : string)  : Observable<{added : boolean}>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    const postUrl = this.urls.operation + "/"+ this.currentDoctor.id;
    //const postUrl = this.urls.operation + "/10";
    const body = {"datum" : datum, "jmbg_pacijenta" : JMBG, "tip_zahvata" : tipZahvata, "propratni_pomocni_zahvar" : propratniZahvat};

    return  this.http.post<{added : boolean}>(postUrl, body, {headers});

  }

  public addBiopsijaInfo(datum : string, JMBG : string, tip : string, histopatoloskevr : string, tiptumora : string, kodklinickogstanja : string){
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    
    //Treba ovako da izgleda ali za sada ovako izgleda
    const body = {"datum" : datum, "tip" : tip, "histopatoloske_vrednosti" : histopatoloskevr, "tip_tumora" : tiptumora, "kod_klinickog_stanja" : kodklinickogstanja, "JMBG" : JMBG};
    //const body = {"datum" : datum, "tip" : tip, "histopatoloske_vrednosti" : histopatoloskevr, "tip_tumora" : tiptumora, "kod_klinickog_stanja" : kodklinickogstanja};
    return this.http.post<{added : boolean}>(this.urls.biopsija, body, {headers});
  }

  public addTerapijaInfo(JMBG : string, tipterapije : string, brojciklusa : string ,grupalekova : string){
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    const postUrl = this.urls.terapija + "/"+ this.currentDoctor.id;

    //Treba ovako d ase salje ali server trenutno nema takav api
    const body = {"tip" : tipterapije, "broj_ciklusa" : brojciklusa , "grupa_lekova" : grupalekova, "JMBG" : JMBG};
    //const body = {"tip" : tipterapije, "broj_ciklusa" : brojciklusa , "grupa_lekova" : grupalekova};
    return this.http.post<{added : boolean}>(postUrl, body, {headers});
  }

  public getAllDoctors() : Observable<{lista : DoctorData[]}>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtUserService.getToken()}`);

    return this.http.get<{lista : DoctorData[]}>(this.urls.base, {headers});
  }

  public getPatientsChart() : Observable<Chart>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    const getUrl  : string = this.urls.getChart + "/" + this.currentlySeeingPatient;  

    return this.http.get<Chart>(getUrl, {headers});
  }

  public setPatient(JMBG : string) : void {
    this.currentlySeeingPatient = JMBG;
    this.hasPatientAtTheMoment = true;
  }

  public unsetPatient() : void{
    this.currentlySeeingPatient = null;
    this.hasPatientAtTheMoment = false;    
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtDoctorService } from '../common/services/jwt-doctor.service';
import { JwtService } from '../common/services/jwt.service';
import { Appointment } from '../_models/appointment.model';
import { IJwtTokenDataDoctor } from '../_models/jwt-doctor-data';
import { DoctorService } from './doctor.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  private readonly urls = {
    getAppointmentsForDoctor : 'http://localhost:3002/api/zakazivanje/zakazivanja/doktor/datum',
    setAppointment : 'http://localhost:3002/api/zakazivanje',
    zabaeleziPregled : 'http://localhost:3002/api/pregledi/zabelezi',
    sledeciPregled : 'http://localhost:3002/api/zakazivanje/sledece'
  }

  constructor(private http: HttpClient, private jwtUserService : JwtService, private jwtDoctorService : JwtDoctorService) { 

  }

  public getAppointmentsForDoctor(datum : string, ILekara : string) : Observable<{"hasApp" : boolean, "lista" : Appointment[]}>{
    let headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtUserService.getToken()}`);
    

    const body = {"datum" : datum, "ILekara" :  ILekara}
    return this.http.post<{"hasApp" : boolean, "lista" : Appointment[]}>(this.urls.getAppointmentsForDoctor, body, {headers});
  }

  public setAnAppointment(datum : string, ILekara : string) : Observable<{odobreno : boolean}>{
    const  headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtUserService.getToken()}`);
    
    const body = {"datum" : datum,"ILekara" : ILekara};
    return this.http.post<{odobreno : boolean}>(this.urls.setAppointment, body, {headers});

  }

  public getScheduledAppointments(datum : string) : Observable<{"hasApp" : boolean, "lista" : Appointment[]}>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtDoctorService.getToken()}`);

    const payLoad : IJwtTokenDataDoctor = this.jwtDoctorService.getDataFromTokne();
    const body = {"datum" : datum,"ILekara" : payLoad.id};

    return this.http.post<{"hasApp" : boolean, "lista" : Appointment[]}>(this.urls.getAppointmentsForDoctor, body, {headers});
  }

  public addPregled(JMBG :string, anamneza : string, izvestaj : string) : Observable<boolean>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtDoctorService.getToken()}`);
    const body = {"jmbg" : JMBG, "anamneza" : anamneza, "izvestaj" : izvestaj};

    return this.http.post<boolean>(this.urls.zabaeleziPregled, body, {headers});
  }

  public getNextAppointment() : Observable<{datum : string, jmbg : string, id_doktora : string}[]>{
    const headers : HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtUserService.getToken()}`);

    return this.http.get<{datum : string, jmbg : string, id_doktora : string}[]>(this.urls.sledeciPregled, {headers});
  }
}

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { DoctorService } from '../services/doctor.service';
import { UserInfoComponent } from '../user-info/user-info.component';
import { Doctor } from '../_models/doctor.model';

@Component({
  selector: 'app-login-osoblje',
  templateUrl: './login-osoblje.component.html',
  styleUrls: ['./login-osoblje.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginOsobljeComponent implements OnInit, OnDestroy {
  public loginOsobljeForm: FormGroup;
  public returnUrl: string;
  private sub : Subscription;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private doctorService : DoctorService) {
    
     //ne znam da li nam je ovo neophodno? ali svakako radi tako da ne bih dirala
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
     UserInfoComponent.pocetak=true;
   }

  ngOnInit(): void {
    this.loginOsobljeForm = new FormGroup({
      ID : new FormControl('', [Validators.required, Validators.pattern('^[0-9]{10}'), Validators.minLength(10), Validators.maxLength(10)]),
      lozinka : new FormControl('', [Validators.required])
    });
  }

  ngOnDestroy() : void{
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

  public get id(){
    return this.loginOsobljeForm.get('ID');
  }
  public get lozinka(){
    return this.loginOsobljeForm.get('lozinka');
  }

  public submitForm(){
    const data = this.loginOsobljeForm.value;
    
    if(!this.loginOsobljeForm.valid){
      window.alert("Uneli ste pogresne podateke. Pokusajte ponovo");
      return;
    }

    const ObvUser : Observable<Doctor> = this.doctorService.login(data.ID, data.lozinka);
    this.sub = ObvUser.subscribe((doctor : Doctor) => {
      //console.log(doctor);
      if(doctor)
        this.router.navigateByUrl('/doctor/home')
    });

  }

  public idHasErrors() : boolean{
    const error : ValidationErrors =  this.loginOsobljeForm.get("ID").errors;  
    return error != null; 
  }

  public IDErrors() : string[]{
    const errors : ValidationErrors = this.loginOsobljeForm.get("ID").errors;
    if(errors == null){
      return [];
    } 

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti ID");
    }
    if(errors.pattern){
      errorMessages.push("Izgled ID-a nije dobar");
    }
    if(errors.minlength){
      errorMessages.push("ID mora da ima 10 karaktera. Uneto je " + errors.minlength.actualLength );
    }
    if(errors.maxlength){
      errorMessages.push("ID mora da ima 10 karaktera. Uneto je " + errors.maxlength.actualLength );
    }

    return errorMessages;
  }


    public lozinkaHasErrors() : boolean{
      const error : ValidationErrors =  this.loginOsobljeForm.get("lozinka").errors;  
      return error != null; 
    }
  
    public lozinkaErrors() : string[]{
      const errors : ValidationErrors = this.loginOsobljeForm.get("lozinka").errors;
      if(errors == null){
        return [];
      } 
  
      const errorMessages : string[] = [];
      if(errors.required){
        errorMessages.push("Morate uneti lozinku");
      }
    return errorMessages;
  }
}

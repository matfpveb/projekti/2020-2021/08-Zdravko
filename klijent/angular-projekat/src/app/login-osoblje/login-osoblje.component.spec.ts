import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginOsobljeComponent } from './login-osoblje.component';

describe('LoginOsobljeComponent', () => {
  let component: LoginOsobljeComponent;
  let fixture: ComponentFixture<LoginOsobljeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginOsobljeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginOsobljeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

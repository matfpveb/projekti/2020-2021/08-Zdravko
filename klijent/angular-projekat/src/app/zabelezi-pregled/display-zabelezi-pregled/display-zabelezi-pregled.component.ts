import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { DoctorService } from 'src/app/services/doctor.service';
//declare const $: any;

@Component({
  selector: 'app-display-zabelezi-pregled',
  templateUrl: './display-zabelezi-pregled.component.html',
  styleUrls: ['./display-zabelezi-pregled.component.css']
})
export class DisplayZabeleziPregledComponent implements OnInit {

  public OperacijaForm: FormGroup;
  public BiopsijaForm: FormGroup;
  public TerapijaForm: FormGroup;
  public opcija: Number=1;
  private subOperation : Subscription;

  constructor(private formBuilder: FormBuilder, private doctorService : DoctorService) {
   }

  ngOnInit(): void {
    this.OperacijaForm = new FormGroup({
      datum : new FormControl('', [Validators.required]),
      JMBG : new FormControl('', [Validators.required, Validators.minLength(13), Validators.maxLength(13)]),
      zahvat : new FormControl('', [Validators.required]),
      pomocnizahvat : new FormControl('', [Validators.required])
    });
    this.BiopsijaForm = new FormGroup({
      tip : new FormControl('', [Validators.required]),
      datum : new FormControl('', [Validators.required]),
      JMBG : new FormControl('', [Validators.required, Validators.minLength(13), Validators.maxLength(13)]),
      histopatoloskevr : new FormControl('', [Validators.required]),
      tiptumora : new FormControl('', [Validators.required]),
      kodklinickogstanja: new FormControl('', [Validators.required]),
    });
    this.TerapijaForm = new FormGroup({
      tipterapija: new FormControl('', [Validators.required]),
      brojciklusa: new FormControl('', [Validators.required]),
      grupalekova: new FormControl('', [Validators.required]),
      JMBG : new FormControl('', [Validators.required, Validators.minLength(13), Validators.maxLength(13)]),
    });
 
    //$(".menu .item").tab();
  }
  //---------------OPERACIJA-------------  
  public submitOperacijaForm(){
    const data = this.OperacijaForm.value;
    //console.log(data);

    if(!this.OperacijaForm.valid){
      window.alert("Formular nije pravilno popunjen. Pokusajte ponovo");
      return;
    }
    const datum = data.datum;
    const jmbg = data.JMBG;
    const tipZahvata = data.zahvat;
    const propratniZahvat = data.pomocnizahvat;
    
    this.subOperation = this.doctorService.addOperationInfo(datum, jmbg, tipZahvata, propratniZahvat).subscribe((response : {added : boolean}) => {
      //window.alert(response.added);
      if(response.added){
        window.alert("Informacije su unete");
      }
      else{
        window.alert("Zahtev nije upamcen. Probajte ponovo");
        this.OperacijaForm.reset();
      }
    });

    this.OperacijaForm.reset();
    //window.alert("Unete su informacije o operaciji!");
  }

  public JMBGHasErrors() : boolean{
    if(this.opcija == 1){
      const error : ValidationErrors = this.OperacijaForm.get("JMBG").errors;
      return error != null;
    }else if(this.opcija == 2){
      const error : ValidationErrors = this.BiopsijaForm.get("JMBG").errors;
      return error != null;
    }else{
      const error : ValidationErrors = this.TerapijaForm.get("JMBG").errors;
      return error != null;
    }
    
  }

  public JMBGErrors() : string []{
    let errors  : ValidationErrors = null;
    if(this.opcija == 1){
      errors = this.OperacijaForm.get("JMBG").errors;
    }else if(this.opcija == 2){
      errors = this.BiopsijaForm.get("JMBG").errors;
    }else{
      errors = this.TerapijaForm.get("JMBG").errors;
    }
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti JMBG");
    }
    if(errors.pattern){
      errorMessages.push("Izgled JMBG-a nije dobar")
    }
    if(errors.minlength){
      errorMessages.push("JMBG mora da ima 13 karakter. Uneto je " + errors.minlength.actualLength);
    }
    if(errors.maxlength){
      errorMessages.push("JMBG mora da ima 13 karaktera. Uneto je " + errors.maxlength.actualLength);
      
    }

    return errorMessages;
  }

  public tipOperacijeHasError() : boolean{
    const error : ValidationErrors = this.OperacijaForm.get("zahvat").errors;
    return error != null;
  }

  public tipOperacijeErrors() : string []{
    const errors : ValidationErrors = this.OperacijaForm.get("zahvat").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti tip zahvata");
    }

    return errorMessages;
  }

  public pomocniOperacijaHasErrors() : boolean{
    const error : ValidationErrors = this.OperacijaForm.get("pomocnizahvat").errors;
    return error != null;
  }

  public pomocniOPeracijaErrors() : string[]{
    const errors : ValidationErrors = this.OperacijaForm.get("pomocnizahvat").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ukoliko nije neophodan unesite -");
    }

    return errorMessages;
  }

  //-------------------BIOPSIJA-------------------

  public submitBiopsijaForm(){
    const data = this.BiopsijaForm.value;
    //console.log(data);

    if(!this.BiopsijaForm.valid){
      window.alert("Formular nije pravilno popunjen. Pokusajte ponovo");
      return;
    }
    const datum = data.datum;
    const jmbg = data.JMBG;
    const tip = data.tip;
    const histopatoloskevr = data.histopatoloskevr;
    const tiptumora = data.tiptumora;
    const kodklinickogstanja = data.kodklinickogstanja;
    
    this.subOperation = this.doctorService.addBiopsijaInfo(datum, jmbg, tip, histopatoloskevr, tiptumora, kodklinickogstanja).subscribe((response : {added : boolean}) => {
      //window.alert(response.added);git 
      if(response.added){
        window.alert("Informacije su unete");
      }
      else{
        window.alert("Zahtev nije upamcen. Probajte ponovo");
        this.OperacijaForm.reset();
      }
    });

    this.OperacijaForm.reset();
    //window.alert("Unete su informacije o operaciji!");
  }

  public tipBiopsijeHasErrors() : boolean{
    const error : ValidationErrors = this.BiopsijaForm.get("tip").errors;
    return error != null;
  }

  public tipBiopsijeErrors() : string[]{
    const errors : ValidationErrors = this.BiopsijaForm.get("tip").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ovo polje je obavezno");
    }

    return errorMessages;  
  }

  public histopatHasErrors() : boolean{
    const error : ValidationErrors = this.BiopsijaForm.get("histopatoloskevr").errors;
    return error != null;
  }

  public histopatErrors() : string[]{
    const errors : ValidationErrors = this.BiopsijaForm.get("histopatoloskevr").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ovo polje je obavezno");
    }

    return errorMessages;  
  }

  public tipTumoraHasErrors() : boolean{
    const error : ValidationErrors = this.BiopsijaForm.get("tiptumora").errors;
    return error != null;
  }

  public tipTumoraErrors() : string[]{
    const errors : ValidationErrors = this.BiopsijaForm.get("tiptumora").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ovo polje je obavezno");
    }

    return errorMessages;  
  }

  public kodStanjaHasErrors() : boolean{
    const error : ValidationErrors = this.BiopsijaForm.get("kodklinickogstanja").errors;
    return error != null;
  }

  public kodStanjaErrors() : string[]{
    const errors : ValidationErrors = this.BiopsijaForm.get("kodklinickogstanja").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ovo polje je obavezno");
    }

    return errorMessages;  
  }

  //----------------TERAPIJA-----------------

  public submitTerapijaForm(){
    const data = this.TerapijaForm.value;
    //console.log(data);

    if(!this.TerapijaForm.valid){
      window.alert("Formular nije pravilno popunjen. Pokusajte ponovo");
      return;
    }
    const tipterapija = data.tipterapija;
    const jmbg = data.JMBG;
    const brojciklusa = data.brojciklusa;
    const grupalekova = data.grupalekova;
    
    this.subOperation = this.doctorService.addTerapijaInfo(jmbg, tipterapija, brojciklusa, grupalekova).subscribe((response : {added : boolean}) => {
      //window.alert(response.added);
      if(response.added){
        window.alert("Informacije su unete");
      }
      else{
        window.alert("Zahtev nije upamcen. Probajte ponovo");
        this.OperacijaForm.reset();
      }
    });

    this.OperacijaForm.reset();
    //window.alert("Unete su informacije o operaciji!");
  }

  public tipTerapijaHasErrors() : boolean{
    const error : ValidationErrors = this.TerapijaForm.get("tipterapija").errors;
    return error != null;
  }

  public tipTerapijaErrors() : string[]{
    const errors : ValidationErrors = this.TerapijaForm.get("tipterapija").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ovo polje je obavezno");
    }

    return errorMessages;  
  }

  public lekoviHasErrors() : boolean{
    const error : ValidationErrors = this.TerapijaForm.get("grupalekova").errors;
    return error != null;
  }

  public lekoviErrors() : string[]{
    const errors : ValidationErrors = this.TerapijaForm.get("grupalekova").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ovo polje je obavezno");
    }

    return errorMessages;  
  }

  public ciklusiHasErrors() : boolean{
    const error : ValidationErrors = this.TerapijaForm.get("brojciklusa").errors;
    return error != null;
  }

  public ciklusiErrors() : string[]{
    const errors : ValidationErrors = this.TerapijaForm.get("brojciklusa").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Ovo polje je obavezno");
    }

    return errorMessages;  
  }


  public PromeniOpciju(op:Number){
    this.opcija = op;
    // /console.log("Menjam opciju");
  }

}

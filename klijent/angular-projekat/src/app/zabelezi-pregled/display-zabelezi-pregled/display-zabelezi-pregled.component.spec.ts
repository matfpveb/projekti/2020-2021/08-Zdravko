import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayZabeleziPregledComponent } from './display-zabelezi-pregled.component';

describe('DisplayZabeleziPregledComponent', () => {
  let component: DisplayZabeleziPregledComponent;
  let fixture: ComponentFixture<DisplayZabeleziPregledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayZabeleziPregledComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayZabeleziPregledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZabeleziPregledComponent } from './zabelezi-pregled.component';

describe('ZabeleziPregledComponent', () => {
  let component: ZabeleziPregledComponent;
  let fixture: ComponentFixture<ZabeleziPregledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZabeleziPregledComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZabeleziPregledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

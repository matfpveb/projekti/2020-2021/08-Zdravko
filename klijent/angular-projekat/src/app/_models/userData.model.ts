export class UserData {
    constructor(
            public jmbg: string,
            public lbo : string,
            public ime: string,
            public prezime: string,
            public adresa : string,
            public grad : string,
            public godiste : string,
            public telefon : string,
            public pol : string,
            public email : string,
    )
    {}
    
}
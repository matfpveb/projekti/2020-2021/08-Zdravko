export class Chart{
    constructor(
                public jmbg : string,
                public lbo : string,
                public ime: string,
                public prezime: string,
                public  adresa: string,
                public grad : string,
                public godiste : string,
                public telefon : string,
                public pol : string,
                public email : string,
                public id : string, //inace broj kartona
                public id_doktora : string,
                public tip_biopsije : string,
                public datum_biopsije : string,
                public tip_terapije : string,
                public alergije : string,
                public datum_dijagnoze_tumora: string
        ){

        }
}
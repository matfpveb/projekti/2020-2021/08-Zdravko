export class Appointment {
    constructor(
            public jmbg: string,
            public ime: string,
            public prezime: string,
            public datum : string,
            public  id_doktora : string
    )
    {}
    
}
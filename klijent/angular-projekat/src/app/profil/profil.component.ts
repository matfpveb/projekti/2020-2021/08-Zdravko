import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from '../services/user.service';
import { User } from '../_models/user.model';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  
  public user : User = null;
  private sub : Subscription;

  constructor(private uService : UserService) { 
    this.sub = this.uService.user.subscribe((user : User) => {
      this.user = user;
    });
  }

  ngOnInit(): void {
  }

}

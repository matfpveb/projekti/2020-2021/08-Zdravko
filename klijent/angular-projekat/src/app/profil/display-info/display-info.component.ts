import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/_models/user.model';
import { UserData } from 'src/app/_models/userData.model';

@Component({
  selector: 'app-display-info',
  templateUrl: './display-info.component.html',
  styleUrls: ['./display-info.component.css']
})
export class DisplayInfoComponent implements OnInit, OnDestroy {

  private sub : Subscription;
  private subUser : Subscription;
  private editSub : Subscription;

  public user : User = null;
  public userData : UserData;
  public change: Boolean = false;
  public ProfilForm : FormGroup;

  constructor(private uService : UserService) {
    this.subUser = this.uService.user.subscribe((user : User) =>{
      this.user = user;
    });
    this.uService.sendUserDataIfExists();

    this.sub= this.uService.getUserInfo().subscribe((uData : UserData) => {
      this.userData = uData;
      this.initializeFormGroup(uData.ime, uData.prezime, uData.email, uData.grad, uData.adresa, uData.telefon);
      //console.log(this.userData);
    });
  }

  ngOnInit(): void {
  }

  public initializeFormGroup(uIme : string, uPrezime :string, uEmail : string, uGrad :string, uAdresa: string, uTelefon : string) :void{
    this.ProfilForm = new FormGroup({
      ime : new FormControl( uIme, [Validators.required]),
      prezime : new FormControl(uPrezime, [Validators.required]),
      email : new FormControl(uEmail, [Validators.required]),
      grad : new FormControl(uGrad, [Validators.required]),
      adresa : new FormControl(uAdresa, [Validators.required]),
      telefon : new FormControl(uTelefon, [Validators.required])
    });
  }
  


  ngOnDestroy() : void{
    if(this.sub){
      this.sub.unsubscribe();
    }

    if(this.subUser){
      this.subUser.unsubscribe();
    }

    if(this.editSub){
      this.editSub.unsubscribe();
    }
  }
 
  public azurirajPacijenta(): void{
    this.change = true;
  }

  public sacuvajIzmene(): void{
    if(this.ProfilForm.invalid){
      window.alert("Formular je neispravan. Sva polja su obavezna, proverite da li ste sve popunili.");  
      return ;
    }
    
    const data = this.ProfilForm.value;
    console.log(this.userData.jmbg + " " + this.userData.lbo, data.ime + " " +
      data.prezime+ " " +data.adresa+ " " +data.grad+ " " +this.userData.godiste+ " " +data.telefon+ " " +this.userData.pol+ " " +data.email);
    this.editSub = this.uService.editUserData(this.userData.jmbg, this.userData.lbo, data.ime, 
      data.prezime, data.adresa, data.grad, this.userData.godiste, data.telefon, this.userData.pol, data.email).subscribe((response : boolean) =>{
        if(response){
          window.alert("Izmene sacuvane ");
        }
        this.sub= this.uService.getUserInfo().subscribe((uData : UserData) => {
          this.userData = uData;
          this.initializeFormGroup(uData.ime, uData.prezime, uData.email, uData.grad, uData.adresa, uData.telefon);
          //console.log(this.userData);
        });
      })
    this.change = false;
  }

}

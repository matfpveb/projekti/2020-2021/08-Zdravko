import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UserService } from '../services/user.service';
import { UserInfoComponent } from '../user-info/user-info.component';
import { User } from '../_models/user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit , OnDestroy{

  public registerForm: FormGroup;
  public returnUrl: string;
  public user: User;
  public userSub : Subscription;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private userService : UserService) {
     //ne znam da li nam je ovo neophodno? ali svakako radi tako da ne bih dirala
     this.user = new User("", "", "");
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
     UserInfoComponent.pocetak=false;
   }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
        LBO : new FormControl('', [Validators.required, Validators.pattern('^[0-9]{11}$'), Validators.minLength(11), Validators.maxLength(11)]),
        email: new FormControl('', [Validators.required, Validators.email]),
        ime : new FormControl('', [Validators.required]),
        prezime : new FormControl('', [Validators.required]),
        godiste : new FormControl('', [Validators.required, Validators.pattern('^[1-9][0-9][0-9][0-9]$')]),
        JMBG : new FormControl('', [Validators.required, Validators.pattern('^[0-9]{13}$'), Validators.minLength(13), Validators.maxLength(13)]),
        adresa : new FormControl('',[Validators.required]),
        grad : new FormControl('', [Validators.required]),
        telefon : new FormControl('', [Validators.required, Validators.minLength(7)]),
        pol : new FormControl('', [Validators.required]),
        doktor : new FormControl('', [Validators.required]),
        alergije : new FormControl('', [Validators.required]),
        lozinka : new FormControl('', [Validators.required])
    });
  }

  ngOnDestroy() : void{
    if(this.userSub){
      this.userSub.unsubscribe();
    }
  }

  public onSubmitForm() : void{
    const data = this.registerForm.value;
    this.user.JMBG = data.JMBG;
    this.user.ime = data.ime;
    this.user.prezime = data.prezime;
  
    if(!this.registerForm.valid){
      window.alert("Formular nije pravilno popunjen. Pokusajte ponovo");
      return;
    }

    this.registerForm.reset();
    //console.log(data);
    const ObsUser : Observable<User> = this.userService.register(data.LBO, data.JMBG, data.ime, data.prezime, data.adresa, data.grad,data.telefon, data.pol, data.email,"-", data.lozinka, data.doktor, data.alergije, data.godiste);
    const sub : Subscription = ObsUser.subscribe((user: User) =>{
      if(user)
        window.alert("Korisnik je formiran");
        this.userService.logout();
        this.backToHomePage();
    });

    this.userSub = sub;
  }

  public backToHomePage() : void{
    this.router.navigateByUrl('doctor/home');
  }

  public jmbgHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("JMBG").errors;
    return error != null;
  }
  
  public JMBGErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("JMBG").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti JMBG");
    }
    if(errors.pattern){
      errorMessages.push("Izgled JMBG-a nije dobar")
    }
    if(errors.minlength){
      errorMessages.push("JMBG mora da ima 13 karakter. Uneto je " + errors.minlength.actualLength);
    }
    if(errors.maxlength){
      errorMessages.push("JMBG mora da ima 13 karaktera. Uneto je " + errors.maxlength.actualLength);
      
    }

    return errorMessages;
  }

  public LBOHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("LBO").errors;
    return error != null;
  }

  public LBOErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("LBO").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti LBO");
    }
    if(errors.pattern){
      errorMessages.push("Izgled LBO-a nije dobar")
    }
    if(errors.minlength){
      errorMessages.push("LBO mora da ima 11 karakter. Uneto je " + errors.minlength.actualLength);
    }
    if(errors.maxlength){
      errorMessages.push("LBO mora da ima 11 karaktera. Uneto je " + errors.maxlength.actualLength);
      
    }
    
    return errorMessages;
  }

  public emailHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("email").errors;
    return error != null;
  }
  
  public emailErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("email").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti email");
    }
    if(errors.email){
      errorMessages.push("Email nije ispravan");
    }

    
    return errorMessages;
  }

  public imeHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("ime").errors;
    return error != null;
  }
  
  public imeErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("ime").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti ime");
    }    
    return errorMessages;
  }

  public prezimeHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("prezime").errors;
    return error != null;
  }
  
  public prezimeErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("prezime").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti prezime");
    }    
    return errorMessages;
  }

  public adresaHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("adresa").errors;
    return error != null;
  }
  
  public adresaErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("adresa").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti adresu");
    }

    return errorMessages;
  }


  public gradHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("grad").errors;
    return error != null;
  }
  
  public gradErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("grad").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti grad");
    }
    return errorMessages;
  }


  public godisteHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("godiste").errors;
    return error != null;
  }
  
  public godisteErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("godiste").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti godiste");
    }
    if(errors.pattern){
      errorMessages.push("Izgled godista nije dobar")
    }

    return errorMessages;
  }

  public telefonHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("telefon").errors;
    return error != null;
  }
  
  public telefonErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("telefon").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti telefon");
    }
    if(errors.minlength){
      errorMessages.push("Neispravan oblik");
    }
    return errorMessages;
  }

  public polHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("pol").errors;
    return error != null;
  }
  
  public polErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("pol").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti pol");
    }
    return errorMessages;
  }

  public doktorHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("doktor").errors;
    return error != null;
  }
  
  public doktorErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("doktor").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti doktora");
    }
    return errorMessages;
  }

  public alergijeHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("alergije").errors;
    return error != null;
  }
  
  public alergijeErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("alergije").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti alergije, ako ih nemate unesite -");
    }
    return errorMessages;
  }


  public lozinkaHasErrors() : boolean{
    const error : ValidationErrors = this.registerForm.get("lozinka").errors;
    return error != null;
  }
  
  public lozinkaErrors() : string []{
    const errors : ValidationErrors = this.registerForm.get("lozinka").errors;
    if(errors == null){
      return [];
    }

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti lozinku");
    }
    return errorMessages;
  }
  get validFormular():boolean{
    return this.registerForm.valid;
  }
}

import { Component, Input } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UserService } from './services/user.service';
import { User } from './_models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Zdravko';
  
  constructor(public userService: UserService, private titleService : Title){
    this.titleService.setTitle("Zdravko");
  }
}

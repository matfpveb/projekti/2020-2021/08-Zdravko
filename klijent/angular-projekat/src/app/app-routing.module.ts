import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfilComponent } from './profil/profil.component';
import { ZahteviComponent } from './zahtevi/zahtevi.component';
import { OdobreniZahteviComponent } from './odobreni-zahtevi/odobreni-zahtevi.component';
import { PromenaLekaraComponent } from './promena-lekara/promena-lekara.component';
import { KartoniComponent } from './kartoni/kartoni.component';
import { RasporedComponent } from './raspored/raspored.component';
import { RegisterComponent } from './register/register.component';
import { ZabeleziPregledComponent } from './zabelezi-pregled/zabelezi-pregled.component';
import { LoginOsobljeComponent } from './login-osoblje/login-osoblje.component';
import { DoktorComponent } from './doktor/doktor.component';
import { UserAuthenticatedGuard } from './gards/user-authenticated.guard';
import { DoctorAuthenticatedGuard } from './gards/doctor-authenticated.guard';
import { ProfilRasporedComponent } from './profil-raspored/profil-raspored.component';

const routes: Routes = [
  //Rutiranje za pacijente
  { path: "", component: LoginComponent },
  { path : 'login', component : LoginComponent},
  { path: 'home', component: HomeComponent, canActivate : [UserAuthenticatedGuard] },
  { path: 'profil', component: ProfilComponent, canActivate : [UserAuthenticatedGuard]},
  { path: 'zahtevi', component: ZahteviComponent, canActivate : [UserAuthenticatedGuard]},
  { path: 'sledeci_pregled', component: OdobreniZahteviComponent, canActivate : [UserAuthenticatedGuard]}, 
  { path: 'promena_lekara', component: PromenaLekaraComponent, canActivate : [UserAuthenticatedGuard]},
  //Rutiranje za doktora
  { path: 'doctor', component : LoginOsobljeComponent},
  { path : 'doctor/home', component : DoktorComponent, canActivate: [DoctorAuthenticatedGuard]},
  { path: 'doctor/kartoni', component: KartoniComponent, canActivate: [DoctorAuthenticatedGuard]},
  { path: 'doctor/raspored', component: RasporedComponent, canActivate: [DoctorAuthenticatedGuard]},
  { path: 'doctor/register', component: RegisterComponent, canActivate: [DoctorAuthenticatedGuard]},
  { path: 'doctor/zabelezi-pregled', component: ZabeleziPregledComponent, canActivate: [DoctorAuthenticatedGuard]},
  { path : 'doctor/see-patient', component : ProfilRasporedComponent, canActivate : [DoctorAuthenticatedGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


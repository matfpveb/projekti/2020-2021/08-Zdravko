import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayZahteviComponent } from './display-zahtevi.component';

describe('DisplayZahteviComponent', () => {
  let component: DisplayZahteviComponent;
  let fixture: ComponentFixture<DisplayZahteviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayZahteviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayZahteviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

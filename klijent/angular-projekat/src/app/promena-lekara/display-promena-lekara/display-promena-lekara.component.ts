import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { DoctorService } from 'src/app/services/doctor.service';
import { UserService } from 'src/app/services/user.service';
import { DoctorData } from 'src/app/_models/docotr-data.model';


@Component({
  selector: 'app-display-promena-lekara',
  templateUrl: './display-promena-lekara.component.html',
  styleUrls: ['./display-promena-lekara.component.css']
})
export class DisplayPromenaLekaraComponent implements OnInit, OnDestroy {

  private doctorSub : Subscription;
  private userSub : Subscription;

  public lekari : DoctorData[];
  public PromenaLekaraForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private dService : DoctorService, private userService : UserService) {
    this.doctorSub = this.dService.getAllDoctors().subscribe((response : {lista : DoctorData[]}) => {
      this.lekari = response.lista;
     // console.log(this.lekari);
    });
  }

  ngOnInit(): void {
    this.PromenaLekaraForm = new FormGroup({
      lekar : new FormControl('', [Validators.required])
    });
  }

  ngOnDestroy() : void {
    if(this.doctorSub){
      this.doctorSub.unsubscribe();
    }
    if(this.userSub){
      this.userSub.unsubscribe();
    }
  }

  public submitForm(){
    const data : {lekar : string}= this.PromenaLekaraForm.value;
    console.log(data.lekar);
    const ILekara :  string = data.lekar;
    
    this.userSub = this.userService.changeDoctor(ILekara).subscribe((response : {changed : boolean}) => {
      if(response.changed){
        window.alert("Lekar je uspesno promenjen!")  
      }
    });

    this.PromenaLekaraForm.reset();
    //window.alert("Zahtev za promenu lekara poslat!");
  }

  
}

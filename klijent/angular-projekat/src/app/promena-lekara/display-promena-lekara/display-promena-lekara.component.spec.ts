import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayPromenaLekaraComponent } from './display-promena-lekara.component';

describe('DisplayPromenaLekaraComponent', () => {
  let component: DisplayPromenaLekaraComponent;
  let fixture: ComponentFixture<DisplayPromenaLekaraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayPromenaLekaraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayPromenaLekaraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromenaLekaraComponent } from './promena-lekara.component';

describe('PromenaLekaraComponent', () => {
  let component: PromenaLekaraComponent;
  let fixture: ComponentFixture<PromenaLekaraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromenaLekaraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromenaLekaraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

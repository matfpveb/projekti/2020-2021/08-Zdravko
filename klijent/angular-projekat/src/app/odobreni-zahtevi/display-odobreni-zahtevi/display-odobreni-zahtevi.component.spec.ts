import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayOdobreniZahteviComponent } from './display-odobreni-zahtevi.component';

describe('DisplayOdobreniZahteviComponent', () => {
  let component: DisplayOdobreniZahteviComponent;
  let fixture: ComponentFixture<DisplayOdobreniZahteviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayOdobreniZahteviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayOdobreniZahteviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

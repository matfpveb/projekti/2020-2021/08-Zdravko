import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppointmentService } from 'src/app/services/appointment.service';

@Component({
  selector: 'app-display-odobreni-zahtevi',
  templateUrl: './display-odobreni-zahtevi.component.html',
  styleUrls: ['./display-odobreni-zahtevi.component.css']
})
export class DisplayOdobreniZahteviComponent implements OnInit, OnDestroy {

  private appSub : Subscription;
  public datum : string = "";

  private dan : string;
  private mesec : string;
  private godina : string;

  constructor(private appService : AppointmentService) {
    this.appSub = this.appService.getNextAppointment().subscribe((response : {datum : string, jmbg : string, id_doktora : string}[])=>{
      this.dan = response[0].datum.substring(8, 10);
      this.mesec = response[0].datum.substring(5, 7);
      this.godina = response[0].datum.substring(0,4);

      this.datum = this.dan + "/" + this.mesec + "/" + this.godina; 
      
      //console.log(response[0].datum.substring(8,10));
    });
   }

  ngOnInit(): void {
  }

  ngOnDestroy() : void{
    if(this.appSub){
      this.appSub.unsubscribe();
    }
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OdobreniZahteviComponent } from './odobreni-zahtevi.component';

describe('OdobreniZahteviComponent', () => {
  let component: OdobreniZahteviComponent;
  let fixture: ComponentFixture<OdobreniZahteviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OdobreniZahteviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OdobreniZahteviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayDoktorComponent } from './display-doktor.component';

describe('DisplayDoktorComponent', () => {
  let component: DisplayDoktorComponent;
  let fixture: ComponentFixture<DisplayDoktorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayDoktorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayDoktorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

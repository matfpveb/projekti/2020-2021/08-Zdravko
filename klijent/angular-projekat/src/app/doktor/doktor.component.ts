import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserInfoComponent } from '../user-info/user-info.component';

@Component({
  selector: 'app-doktor',
  templateUrl: './doktor.component.html',
  styleUrls: ['./doktor.component.css']
})
export class DoktorComponent implements OnInit {

  constructor(private router : Router) {
    UserInfoComponent.pocetak=false;
   }

  ngOnInit(): void {
  }

}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DoctorService } from 'src/app/services/doctor.service';
import { UserInfoComponent } from 'src/app/user-info/user-info.component';
import { Doctor } from 'src/app/_models/doctor.model';

@Component({
  selector: 'app-navigation-doktor',
  templateUrl: './navigation-doktor.component.html',
  styleUrls: ['./navigation-doktor.component.css']
})
export class NavigationDoktorComponent implements OnInit, OnDestroy {
  public doctor : Doctor = null;
  private sub : Subscription;

  constructor(private dService : DoctorService, private router : Router) {
    this.sub = this.dService.doctor.subscribe((doctor : Doctor) =>
    {
      this.doctor = doctor;
    });
    this.dService.sendUserDataIfExists();
   }
  
  ngOnInit(): void {

  }

  ngOnDestroy() :void{
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

  public logout(){
    this.dService.logout();
    UserInfoComponent.pocetak=true;
  }

}

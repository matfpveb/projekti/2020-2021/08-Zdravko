import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationDoktorComponent } from './navigation-doktor.component';

describe('NavigationDoktorComponent', () => {
  let component: NavigationDoktorComponent;
  let fixture: ComponentFixture<NavigationDoktorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigationDoktorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationDoktorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

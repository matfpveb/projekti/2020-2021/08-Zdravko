import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav-doktor',
  templateUrl: './sidenav-doktor.component.html',
  styleUrls: ['./sidenav-doktor.component.css']
})
export class SidenavDoktorComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }

}

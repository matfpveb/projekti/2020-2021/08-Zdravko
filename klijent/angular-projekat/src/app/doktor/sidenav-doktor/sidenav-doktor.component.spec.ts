import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavDoktorComponent } from './sidenav-doktor.component';

describe('SidenavDoktorComponent', () => {
  let component: SidenavDoktorComponent;
  let fixture: ComponentFixture<SidenavDoktorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidenavDoktorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavDoktorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ProfilComponent } from './profil/profil.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './home/navigation/navigation.component';
import { SidenavComponent } from './home/sidenav/sidenav.component';
import { DisplayComponent } from './home/display/display.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { UserInfoComponent } from './user-info/user-info.component';
import { DisplayInfoComponent } from './profil/display-info/display-info.component';
import { ZahteviComponent } from './zahtevi/zahtevi.component';
import { DisplayZahteviComponent } from './zahtevi/display-zahtevi/display-zahtevi.component';
import { OdobreniZahteviComponent } from './odobreni-zahtevi/odobreni-zahtevi.component';
import { DisplayOdobreniZahteviComponent } from './odobreni-zahtevi/display-odobreni-zahtevi/display-odobreni-zahtevi.component';
import { PromenaLekaraComponent } from './promena-lekara/promena-lekara.component';
import { DisplayPromenaLekaraComponent } from './promena-lekara/display-promena-lekara/display-promena-lekara.component';
import { DoktorComponent } from './doktor/doktor.component';
import { SidenavDoktorComponent } from './doktor/sidenav-doktor/sidenav-doktor.component';
import { NavigationDoktorComponent } from './doktor/navigation-doktor/navigation-doktor.component';
import { DisplayDoktorComponent } from './doktor/display-doktor/display-doktor.component';
import { KartoniComponent } from './kartoni/kartoni.component';
import { DisplayKartoniComponent } from './kartoni/display-kartoni/display-kartoni.component';
import { RasporedComponent } from './raspored/raspored.component';
import { DisplayRasporedComponent } from './raspored/display-raspored/display-raspored.component';
import { LoginOsobljeComponent } from './login-osoblje/login-osoblje.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ZabeleziPregledComponent } from './zabelezi-pregled/zabelezi-pregled.component';
import { DisplayZabeleziPregledComponent } from './zabelezi-pregled/display-zabelezi-pregled/display-zabelezi-pregled.component';
import { ProfilRasporedComponent } from './profil-raspored/profil-raspored.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfilComponent,
    HomeComponent,
    NavigationComponent,
    SidenavComponent,
    DisplayComponent,
    RegisterComponent,
    UserInfoComponent,
    DisplayInfoComponent,
    ZahteviComponent,
    DisplayZahteviComponent,
    OdobreniZahteviComponent,
    DisplayOdobreniZahteviComponent,
    PromenaLekaraComponent,
    DisplayPromenaLekaraComponent,
    DoktorComponent,
    SidenavDoktorComponent,
    NavigationDoktorComponent,
    DisplayDoktorComponent,
    KartoniComponent,
    DisplayKartoniComponent,
    RasporedComponent,
    DisplayRasporedComponent,
    LoginOsobljeComponent,
    CalendarComponent,
    ZabeleziPregledComponent,
    DisplayZabeleziPregledComponent,
    ProfilRasporedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
   ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

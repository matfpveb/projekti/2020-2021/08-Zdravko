import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { User } from '../_models/user.model';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class LoginComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public returnUrl: string;
  private sub : Subscription;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private userService : UserService) {
    
     //ne znam da li nam je ovo neophodno? ali svakako radi tako da ne bih dirala
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
   }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      JMBG : new FormControl('', [Validators.required, Validators.pattern('^[0-9]{13}'), Validators.minLength(13), Validators.maxLength(13)]),
      lozinka : new FormControl('', [Validators.required])
    });
  }

  ngOnDestroy() : void{
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

  public get jmbg(){
    return this.loginForm.get('JMBG');
  }
  public get lozinka(){
    return this.loginForm.get('lozinka');
  }

  public submitForm(){
    const data = this.loginForm.value;
    if(this.loginForm.invalid){
      window.alert("Neipravni podaci. Pokusajte ponovo.")
      return;
    }

    const ObvUser : Observable<User> = this.userService.login(data.JMBG, data.lozinka); 
    this.sub = ObvUser.subscribe((user : User) => {
      //console.log(user);
      if(user)
        this.router.navigateByUrl('/home');
    });
  }

  public jmbgHasErrors() : boolean{
    const error : ValidationErrors =  this.loginForm.get("JMBG").errors;  
    return error != null; 
  }

  public JMBGErrors() : string[]{
    const errors : ValidationErrors = this.loginForm.get("JMBG").errors;
    if(errors == null){
      return [];
    } 

    const errorMessages : string[] = [];
    if(errors.required){
      errorMessages.push("Morate uneti JMBG");
    }
    if(errors.pattern){
      errorMessages.push("Izgled JMBG-a nije dobar");
    }
    if(errors.minlength){
      errorMessages.push("Jmbg mora da ima 13 karaktera. Uneto je " + errors.minlength.actualLength );
    }
    if(errors.maxlength){
      errorMessages.push("Jmbg mora da ima 13 karaktera. Uneto je " + errors.maxlength.actualLength );
    }

    return errorMessages;
  }


    public lozinkaHasErrors() : boolean{
      const error : ValidationErrors =  this.loginForm.get("lozinka").errors;  
      return error != null; 
    }
  
    public lozinkaErrors() : string[]{
      const errors : ValidationErrors = this.loginForm.get("lozinka").errors;
      if(errors == null){
        return [];
      } 
  
      const errorMessages : string[] = [];
      if(errors.required){
        errorMessages.push("Morate uneti lozinku");
      }
    return errorMessages;
  }
}

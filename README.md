# :hospital: Project Zdravko :hospital:

Zdravko je online dom zdravlja koji omogucava laksu interakciju i komunikaciju izmedju pacijenta i njegovog izabranog lekara. Osnovni korisnici nase aplikaciju su:<br>
 :one: Doktor <br>
 :two: Pacijent

## O projektu

Zdravko je osmisljen da bude vrsta informacionog sistema za lekare i pacijente koji se lece u tom domu zdravlja. Zdravko je veb aplikacija koja podrzava dva tipa korisnika, lekare i pacijente.

Registracija lekara mora da se odradi rucno, putem slanja zahteva serveru koristeci neke od alata nmp Postman-a. Lekari koji su zaposleni u domu zdravlja, imaju svoje naloge pomocu kojih se loguju u aplikaciju i vrse preglede. Lekari mogu da: izlistaju kartone svoh svojih pacijenata, izlistaju zakazane pregleda za odabrani datum, i izvrse pregled. Zatim mogu da unesu informacije o terapiji, operaciji ili biopsiji za datog pacijenta. 

Lekar registruje svoje pacijente i na taj nacin im omogucava koriscenje aplikacije. Pacijenti su u mogucnosti da zakazu pregled putem aplikacije kod svog izabranog lekara. Dobiju informacje o prvom sledecem zakazanom terminu, i promene izabranog lekara ako smatrju da nisu zadovoljni svojim lekarom. Pacijent moze pristupi svojim osnovnim informacijama i da ih promeni ukoliko je to neophodno.

## Pokretanje projekta

Arhitektura projekta je klijent-server. Klijent je napisan u razvojnom okruzenju Angular dok je server napisan u razvojnom okruzenju nodeJS. Tako da je neophodno imati instaliran Angular i nodeJS. Baza koju koristi server za cuvanje informacija je mysql tako da je neophodno imati instialiran i mysql. Za inicijalizaciju baze se koriste scriptovi na putanji /server/db/scripts.Model se nalazi na putanji /server/db/model.  Takodje za pokretanje baze neophodno je u direktorijumu server napraviti db.config koji sadrzi username i password baze, svako u zasebnom redu.

## Video snimak aplikacije

[Video](https://drive.google.com/drive/folders/19qxYV6Mip_lwTquFN3ldnaPtjUIfC_wL?usp=sharing)

## Nedeljni izvestaji

[Izvestaji](https://gitlab.com/matfpveb/projekti/2020-2021/08-Zdravko/-/wikis/Home-Page)

## Neophodno za pokretanje

- NodeJs  10.19
- Angular 11.2.12
- MySql 8.0.25

## Developers

:dvd: [Mila Mladjenovic, 100/2017](https://gitlab.com/milamladjenovic)<br>
:dvd: [Andjela Ilic, 105/2017](https://gitlab.com/ilicandjela) <br>
:cd: [Vladimir Arsenijevic, 39/2017](https://gitlab.com/Alienso) <br>
:cd: [Mirko Ilic, 468/2018](https://gitlab.com/crimo)
